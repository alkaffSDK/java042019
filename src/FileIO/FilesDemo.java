package FileIO;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class FilesDemo {

    public static void main(String[] args) {

        String filePathName = "D:\\countries.txt";
        File file = new File(filePathName);

        System.out.println("file.getName() = " + file.getName());
        System.out.println("file.exists() = " + file.exists());
        System.out.println("file.isHidden() = " + file.isHidden());
        System.out.println("file = " + file.getPath());
        System.out.println("file.getTotalSpace() = " + file.getTotalSpace());
        System.out.println("file.getUsableSpace() = " + file.getUsableSpace());
        System.out.println("file.isFile() = " + file.isFile());
        System.out.println("file.isDirectory() = " + file.isDirectory());


        // To read the text files

        int readSize = 0;
//
//        if (file != null && file.exists() && file.isFile()) {
//            try (FileInputStream fis = new FileInputStream(file)) {
//                byte[] buffer = fis.readAllBytes();
//
//                if (buffer != null) {
//                    System.out.print(new String(buffer));
//                }
//
//
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            } catch (SecurityException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//

        System.out.println("===========");
        try {
            Files.lines(Paths.get(filePathName)).forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("===========");

        file = new

                File("D:\\java CMD");

        System.out.println("file.getName() = " + file.getName());
        System.out.println("file.exists() = " + file.exists());
        System.out.println("file.isHidden() = " + file.isHidden());
        System.out.println("file = " + file.getPath());
        System.out.println("file.getTotalSpace() = " + file.getTotalSpace());
        System.out.println("file.getUsableSpace() = " + file.getUsableSpace());
        System.out.println("file.isFile() = " + file.isFile());
        System.out.println("file.isDirectory() = " + file.isDirectory());
        if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                System.out.println("+---" + f.getName());
            }
        }
        System.out.println("file.toString() = " + file.toString());


    }
}
