package FileIO;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class ReadingTextFiles {

    public static void main(String[] args) {


        String filePath = "D:\\data.txt";

        System.out.println("=============readingUsingScanner=============");
        readingUsingScanner(filePath);

        System.out.println("=============readingFileInputStream=============");
        readingFileInputStream(filePath);


        System.out.println("=============readingBufferReader=============");
        readingBufferReader(filePath);

        System.out.println("=============readingWithFileAPI=============");
        readingWithFileAPI(filePath);


    }

    public static void readingUsingScanner(String filePath)
    {
        try (Scanner scanner = new Scanner(new File(filePath), Charset.forName("UTF-8")))
        {
            while(scanner.hasNextLine())
                System.out.println(scanner.nextLine());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void readingFileInputStream(String filePath)
    {
        try (FileInputStream fos   = new FileInputStream(new File(filePath)))
        {
            byte[] bytes ;
            int size = 0 ;
            if (( size = fos.available()) > 0 )
            {
                bytes = fos.readAllBytes() ;
                System.out.print(new String(bytes));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void readingBufferReader(String filePath)
    {
        try (BufferedReader reader = new BufferedReader(new FileReader(new File(filePath))) )
        {
           String line = "";
           while ((line = reader.readLine()) != null)
           {
               System.out.println(line);
           }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }public static void readingWithFileAPI(String filePath)
    {
        try {
            Files.lines(Paths.get(filePath)).forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}
