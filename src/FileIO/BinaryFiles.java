package FileIO;

import java.io.*;
import java.nio.file.AccessDeniedException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class BinaryFiles {


    public static void main(String[] args) {
//        try {
//            copy("F:\\Users\\a.alkaff\\Pictures\\android3.jpg","D:\\Ahmed\\android3.jpg");
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//
//        } catch (AccessDeniedException e) {
//            e.printStackTrace();
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        copyJava7("F:\\Users\\a.alkaff\\Pictures\\android3.jpg","D:\\Ahmed\\android4.jpg");
    }


    public static  void copy(String src, String trg) throws IOException {
        File source = new File(src);
        System.out.println("source.getName() = " + source.getName());
        if(! source.exists())
            throw  new FileNotFoundException();

        if(! source.canRead())
            throw  new AccessDeniedException("Can't read from the file:"+source.getAbsolutePath());

        File target = new File(trg);

        if(!target.getName().contains("."))
        {
            if(target.mkdirs())
            {
                target.setWritable(true);
                target =  new File(target,source.getName());
                target.setWritable(true);
            }
        }else{
            if(!target.canWrite())
                throw  new AccessDeniedException("Can't write to the :"+target.getAbsolutePath());
        }



        int readSize, bufferSize ;
        try(FileInputStream fis = new FileInputStream(source);
            FileOutputStream fos =  new FileOutputStream(target))
        {
            bufferSize = -1 ;
            System.out.println("bufferSize = " + bufferSize);
            byte[] buffer = new byte[bufferSize > 0 ? bufferSize : 1024] ;
            while((readSize =  fis.read(buffer))  > 0)
            {
                System.out.println("readSize = " + readSize);
                fos.write(buffer,0,readSize);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static  void copyJava7(String src, String trg) {
        try {
            Files.copy(Paths.get(src), Paths.get(trg), StandardCopyOption.COPY_ATTRIBUTES);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
