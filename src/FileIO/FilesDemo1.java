package FileIO;

import java.io.File;

public class FilesDemo1 {

    private static String PREFIX = "  ";

    public static void main(String[] args) {

        printFileNames("","D:\\Java 2019");

        System.out.println();

        String filePath = "D:\\Test.sql";
        File file = new File(filePath);
        System.out.println("file.canExecute() = " + file.canExecute());
        System.out.println("file.canRead() = " + file.canRead());
        System.out.println("file.canWrite() = " + file.canWrite());
        System.out.println("file.getAbsolutePath() = " + file.getAbsolutePath());
        System.out.println("file.getName() = " + file.getName());
        System.out.println("file.getParent() = " + file.getParent());
        System.out.println("file.getPath() = " + file.getPath());
        System.out.println("file.isFile() = " + file.isFile());
        System.out.println("file.isDirectory() = " + file.isDirectory());
        System.out.println("file.isHidden() = " + file.isHidden());
        System.out.println("file.exists() = " + file.exists());
    }

    public static void printFileNames(String prefix, String path)
    {
        try {
            File f = new File(path);
            if(f.exists())
            {
                if(f.isFile())
                    System.out.println(prefix + f.getName());
                else{
                    System.out.println(prefix + f.getName() + "\t <DIR>");
                    for(File file : f.listFiles())

                        printFileNames(prefix+PREFIX,file.getAbsolutePath());
                }
            }else
                System.err.println("File is not exists.");

        }catch (Exception ex)
        {

        }

    }
}
