package jdbc;

import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

public class ConnectionDemo {

    public static void main(String[] args) {

        // Create a variable for the connection string.
        String connectionUrl = "jdbc:sqlserver://localhost:1433;databaseName=Countries;user=sa;password=123";
        String connectionUrl1 = "jdbc:sqlserver://localhost:1433;databaseName=asp032019;integratedSecurity=true;";
        ResultSet rs ;


        try (Connection con = DriverManager.getConnection(connectionUrl1);
             Statement stmt = con.createStatement();

             Scanner scanner = new Scanner(System.in)) {

            System.out.print("Login :");
            String login =scanner.nextLine().trim() ;

            System.out.print("Password :");
            String pass =scanner.nextLine() ;


            // this is a bad practice since it will allow sql injection to solve this use prepareStatement and parameters

//            String SQL = "SELECT * FROM [Users] WHERE [Login] = \'"+login+"\' AND Password = \'"+pass + "\' ;";
//             rs = stmt.executeQuery(SQL);

            // here is how to use prepareStatement and parameters
            String SQLWithParameters = "SELECT * FROM [Users] WHERE [Login] = ? AND [Password] = ? ;";
            String INSERTWithParameters = "INSERT INTO [Users] ([FirstName], [LastName], [Login], [Password], [SupervisorID]) values (?,?,?,?,?) ;";

            try(PreparedStatement preparedStatement = con.prepareStatement(SQLWithParameters);
                PreparedStatement InsertpreparedStatement = con.prepareStatement(INSERTWithParameters)) {

                InsertpreparedStatement.setString(1,"Majd");
                InsertpreparedStatement.setString(2,"Zahrawi");
                InsertpreparedStatement.setString(3,"m.Zahrawi");
                InsertpreparedStatement.setString(4,"123m");
                InsertpreparedStatement.setInt(5,10);

                int result = InsertpreparedStatement.executeUpdate() ;
                System.out.println("result = " + result);


                preparedStatement.setString(1, login);
                preparedStatement.setString(2, pass);
                rs = preparedStatement.executeQuery();





                // Iterate through the data in the result set and display it.

                ArrayList<String[]> data = new ArrayList<>();
                String[] array;
                int col = rs.getMetaData().getColumnCount();
                int[] len = new int[col];
                int totalLen = col * 2;
                String[] colNames = new String[col];

                for (int i = 0; i < col; i++) {
                    colNames[i] = rs.getMetaData().getColumnName(i + 1);
                    len[i] = colNames[i].length() > len[i] ? colNames[i].length() : len[i];
                }

                String s;
                while (rs.next()) {
                    array = new String[col];
                    for (int i = 0; i < col; i++) {
                        s = rs.getString(i + 1);
                        if (s != null)
                            len[i] = s.length() > len[i] ? s.length() : len[i];
                        array[i] = s;
                    }
                    data.add(array);
                }
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < col; i++) {
                    totalLen += len[i];
                    builder.append("%-").append(len[i]).append("s |");
                }

                String format = builder.append("%n").toString();

                System.out.printf(format, colNames);
                System.out.println(repeat("-", totalLen));

                for (String[] a : data) {
                    System.out.printf(format, a);
                }
            }
        }
        // Handle any errors that may have occurred.
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static String repeat(String s, int totalLen) {
        StringBuilder builder = new StringBuilder();
        for(int i=0;i<totalLen;i++)
            builder.append(s);

        builder.append("");

        return  builder.toString();
    }
}
