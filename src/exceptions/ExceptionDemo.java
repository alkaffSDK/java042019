package exceptions;

import java.util.InputMismatchException;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class ExceptionDemo {


    public static void method() {
        method();
    }

    public static int divide(int a, int b) {

        return a / b;                    // 1
    }

    public static void main(String[] args) {

        // method();

        Scanner scanner = new Scanner(System.in);
        int x = 0, y = 0;
        do {

            try {
                //method();
                System.out.print("X:");
                x = scanner.nextInt();                  // 3
                System.out.print("Y:");
                y = scanner.nextInt();                  // 3
                scanner.nextLine();
                System.out.printf(Locale.getDefault(), "%d / %d = %s%n", x, y, divide(x, y));   // 3
                System.out.println("-------------");

            } catch (ArithmeticException e) {
                System.err.println(e.getMessage());
                System.out.printf(Locale.getDefault(), "%d / %d = \u221E %n", x, y);   // 2
            } catch (InputMismatchException ex) {
                System.err.println(ex + ", " + ex.getMessage());
                x = 0;
                y = 0;
                scanner.nextLine();

            } catch (IllegalStateException | NoSuchElementException ex) {
                // multi catch
                System.err.println(ex.getMessage());
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
                System.out.printf(Locale.getDefault(), "%d / %d = \u221E %n", x, y);
            }catch (Throwable t)
            {
                if(t instanceof Error)
                {
                    System.err.println("It is an Error:"+t.getMessage());
                }else if( t instanceof Exception)
                {
                    System.err.println("It is an Exception:"+t.getMessage());
                    if(t instanceof ArithmeticException)
                        System.out.printf(Locale.getDefault(), "%d / %d = \u221E %n", x, y);   // 2

                    else if( t instanceof IllegalStateException || t instanceof NoSuchElementException)
                        System.err.println("IllegalStateException| NoSuchElementException");
                    else if( t instanceof InputMismatchException)
                        System.err.println("InputMismatchException");
                }else
                    System.err.println(t.getMessage());

            }finally {
                System.out.println("Finally, is block that will be executed if the try block was entered regardless what has happened inside the try or catch blocks");
            }

//            catch (IllegalStateException | NoSuchElementException ex)
//            {
//                // multi catch
//                System.err.println(ex.getMessage());
//            }catch (Exception e)
//            {
//                System.err.println(e.getMessage());
//            }catch (Throwable t)
//            {
//                if(t instanceof Error)
//                {
//                    // Handle errors
//                }else if (t instanceof Exception)
//                {
//                    if(t instanceof RuntimeException)
//                    {
//                        if(t instanceof ArithmeticException)
//                        {
//                        }
//                    }else if(t instanceof IOException)
//                    {
//                    }
//                }
//            }
            System.out.println("Press enter to continue or Q to exit");
        } while (!scanner.nextLine().equalsIgnoreCase("q"));

        System.out.println("Thank you");
        scanner.close();


    }
}
