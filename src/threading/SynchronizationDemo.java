package threading;

public class SynchronizationDemo {

    // synchronized method, with object as a look

    int x  = 0;
    Boolean x_lock = false ;


    static int  y = 0;

    static Boolean y_lock ;

    // both method1 and method2 are synchronized together using the object look,
    // so if any thread  access the first method then no thread can access  any synchronized method (method1 or method2)
    // for this object only
    public synchronized void method1()
    {
        Test1();
    }

    public synchronized void method2()
    {
        Test1();
    }


    // both method3 and method4 are synchronized together using the class look,
    // so if any thread  access the first method then no thread can access  any synchronized method (method3 or method4)
    // for this class
    static  public  synchronized void method3()
    {

        Test();

    }

    static  public  synchronized void method4()
    {
        Test() ;
    }

    public  void Test1()
    {
        try {
            System.out.printf("I'm %s , int the object %s ,I just started...%n", Thread.currentThread().getName(), this);
            Thread.sleep(1000);
            synchronized (this)
            {
                x++ ;
            }

            // or better use a lock object

            synchronized (x_lock)
            {
                x++ ;
            }

            System.out.printf("I'm %s , it just woke up.%n", Thread.currentThread().getName());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void Test()
    {
        try {
            System.out.printf("I'm %s , int the object %s ,I just started...%n", Thread.currentThread().getName());
            Thread.sleep(1000);
            synchronized (SynchronizationDemo.class)
            {
                y++ ;
            }
            // or better

            synchronized (y_lock)
            {
                y++ ;
            }

            System.out.printf("I'm %s , it just woke up.%n", Thread.currentThread().getName());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {

        SynchronizationDemo demo = new SynchronizationDemo();
        SynchronizationDemo demo1 = new SynchronizationDemo();
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    demo1.method2();
                    demo.method1();
                }
            }
        },"Thread 1");

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    demo.method2();
                    demo1.method1();
                }
            }
        },"Thread 2");


        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Done");


    }
}
