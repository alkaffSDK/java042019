package threading;

import javax.swing.*;
import java.util.Locale;

public class ThreadingDemo1 {

    // Thread :

    // Process:


    static class Tester {
        int x = 0;

        Boolean Lock = false;

        public void increase(int counter) {
            System.out.printf("Thread %s is start working..%n", Thread.currentThread().getName());
            for (int i = 0; i < counter; i++) {
                x++;

                //System.out.printf(Locale.getDefault(),"%-15s x:%d%n",Thread.currentThread().getName(),x);
            }
        }

        public void decrease(int counter) {
            System.out.printf("Thread %s is start working..%n", Thread.currentThread().getName());
            for (int i = 0; i < counter; i++) {
                x--;
                //System.out.printf(Locale.getDefault(), "%-15s x:%d%n",Thread.currentThread().getName(),x);
            }
        }
    }

    public static void main(String[] args) {
        int Counter = 10000;
        System.out.println(Thread.currentThread().getName());

//
        Tester tester = new Tester();
//        tester.increase(Counter);
//        System.out.println("After increase x is :" + tester.x);
//        tester.decrease(Counter);
//        System.out.println("After decrease x is :" + tester.x);

        System.out.println(Thread.currentThread().getName() + ": Finally x is :" + tester.x);


        // WithThreading(Counter);
        System.out.println("==============");

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                tester.increase(Counter);
            }
        }, "First Thread");


        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                tester.decrease(Counter);
            }
        }, "Second Thread");

        t.start();
        System.out.println(Thread.currentThread().getName() + ":After starting the first thread x is :" + tester.x);
        t1.start();
        System.out.println(Thread.currentThread().getName() + ":After starting the Second thread x is :" + tester.x);

        try {
            t.join();
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(Thread.currentThread().getName() + ": Finally x is :" + tester.x);
        //  NoThreading(Counter);
    }

    public static void NoThreading(int c) {
        Tester tester = new Tester();
        long start = System.currentTimeMillis();
        tester.increase(c);
        tester.decrease(c);

        long end = System.currentTimeMillis() - start;
        System.out.printf(Locale.getDefault(), "X values is :%-10d with Time:%20d(ms) %n", tester.x, end);
    }

    public static void WithThreading(int c) {
        Tester tester = new Tester();
        long start = System.currentTimeMillis();

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                tester.increase(c);
            }
        }, "First");

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                tester.decrease(c);
            }
        }, "Second");

        t1.start();
        t2.start();


        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long end = System.currentTimeMillis() - start;
        System.out.printf(Locale.getDefault(), "X values is :%-10d with Time:%20d(ms) %n", tester.x, end);
    }
}
