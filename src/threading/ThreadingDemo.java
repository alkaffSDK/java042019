package threading;

import java.util.Locale;
import java.util.Vector;

public class ThreadingDemo {

    static  class Holder {
        Boolean X_Look , Y_Look ;
        int x ;
        int y ;


        public  void increase(int counter)
        {
            System.out.printf(Locale.getDefault() , "Increase is started in %15s thread%n",Thread.currentThread().getName() );
            for (int i = 0; i < counter; i++) {
                synchronized (X_Look) {
                    x++;
                }
                System.out.printf(Locale.getDefault(),"In %15s : x = %d %n",Thread.currentThread().getName() , x);
            }
        }

        public  void decrease(int counter)
        {
            System.out.printf(Locale.getDefault() , "Decrease is started in %15s thread%n",Thread.currentThread().getName() );
            for (int i = 0; i < counter; i++) {
                synchronized (X_Look) {
                    x--;
                }
                System.out.printf(Locale.getDefault(),"In %15s : x = %d %n",Thread.currentThread().getName() , x);
            }
        }

        public  void increaseY(int counter)
        {
            System.out.printf(Locale.getDefault() , "Increase is started in %15s thread%n",Thread.currentThread().getName() );
            for (int i = 0; i < counter; i++) {
                synchronized (Y_Look) {
                    y++;
                }
                System.out.printf(Locale.getDefault(),"In %15s : x = %d %n",Thread.currentThread().getName() , x);
            }
        }

        public  void decreaseY(int counter)
        {
            System.out.printf(Locale.getDefault() , "Decrease is started in %15s thread%n",Thread.currentThread().getName() );
            for (int i = 0; i < counter; i++) {
                synchronized (Y_Look) {
                    y--;
                }
                System.out.printf(Locale.getDefault(),"In %15s : x = %d %n",Thread.currentThread().getName() , x);
            }
        }
    }

    public static void main(String[] args) {

        Holder holder = new Holder();
        int counter = 100000 ;

//        System.out.println("============runWithoutThreading================");
//        runWithoutThreading(holder, counter);
//
//        System.out.printf(Locale.getDefault(),"In %15s thread  x:%d%n",Thread.currentThread().getName(),holder.x);
//        System.out.println(Thread.currentThread().getName() + "#######################################\n");
//

        System.out.println(Thread.currentThread().getName() + "============runWithThreading================");
        runWithThreading(holder, counter);

        System.out.printf(Locale.getDefault(),"In %15s thread  x:%d%n",Thread.currentThread().getName(),holder.x);
        System.out.println(Thread.currentThread().getName() + "#######################################\n");
    }

    private static void runWithThreading(Holder holder, int counter) {

        Thread ft = new Thread(new Runnable() {
            @Override
            public void run() {
                holder.increase(counter);
            }
        },"First");

        Thread st = new Thread(new Runnable() {
            @Override
            public void run() {
                holder.decrease(counter);
            }
        },"Second");

        ft.start();
        st.start();

        // waif for the threads to finish

        try {
            ft.join();
            st.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private static void runWithoutThreading(Holder holder, int counter) {

        holder.increase(counter);
        holder.decrease(counter);
    }
}
