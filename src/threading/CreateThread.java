package threading;

import java.io.*;

public class CreateThread {

    // Method 2: to create a thread
    static class MyFileReader implements Runnable {

        private File mfile ;
        public MyFileReader(String path){
            mfile = new File(path);
        }
        public MyFileReader(File file){
            mfile = file ;
        }

        @Override
        public void run() {
            // TODO : you code come here
            if(mfile != null && mfile.exists() && mfile.isFile())
            {
                try(BufferedReader reader = new BufferedReader(new FileReader(mfile)))
                {
                    String line = "" ;
                    while((line =reader.readLine()) != null)
                    {
                        System.out.println(line);
                    }

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }


    static class FileReaderThread extends Thread {

        private File mFile ;
        public FileReaderThread(String path){mFile = new File(path) ;  start();}
        public FileReaderThread(File file){ mFile =file ; start(); }
//        public MyFileReader1(Runnable runnable,File file){
//            super(runnable);
//            mFile =file ;
//            start(); }
//        public MyFileReader1(Runnable runnable,String file){ this(runnable, new File(file));
//            }

        @Override
        public void run() {
            try(BufferedReader reader = new BufferedReader(new FileReader(mFile)))
            {
                String line = "" ;
                while((line =reader.readLine()) != null)
                {
                    System.out.println(line);
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public static void main(String[] args) {


        // Method 1: to create a thread
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO : you code come here
            }
        });
        thread.start();         // to start the Thread

        // OR

        new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO : you code come here
            }
        }).start();





        // Method 2: to create a thread

//        Thread thread1 = new Thread(new MyFileReader("D:\\newFile.txt"));
//        thread1.start();
//
//        Thread thread2 = new Thread(new MyFileReader("D:\\target.txt"));
//        thread2.start();


        FileReaderThread myFileReader1 = new FileReaderThread("D:\\bad emals.txt");
       // myFileReader1.start();

//        MyFileReader1 myFileReader12 = new MyFileReader1(new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("This is the new runnable");
//            }
//        },"D:\\bad emals.txt");


    }
}
