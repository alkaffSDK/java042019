package threading;

import java.util.Scanner;
import java.util.concurrent.*;

public class ExecutorTask {

    public static void main(String[] args) {

        boolean IsRunning = true ;
        String line = null;
        Scanner scanner= new Scanner(System.in);
        BlockingQueue<String> dataBlockingQueue = new ArrayBlockingQueue<>(10);

        ExecutorService executorService  =  Executors.newFixedThreadPool(2) ;

        Runnable splitter = new Runnable() {
            @Override
            public void run() {
                if(dataBlockingQueue!= null && ! dataBlockingQueue.isEmpty())
                {
                    try {
                        String text = dataBlockingQueue.take() ;
                        for (int i = 0; i < text.length() ; i++) {
                            System.out.printf("Thread :%s ->%c%n",Thread.currentThread().getName(), text.charAt(i));
                            Thread.sleep(1000);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };


        while (! executorService.isShutdown())
        {
            line = scanner.nextLine() ;


            if(line != null && ! line.isEmpty() && ! line.isBlank())
            {
                if(!line.equalsIgnoreCase("Shutdown")) {
                    dataBlockingQueue.add(line);
                    executorService.submit(splitter) ;
                }
                else
                    executorService.shutdown();
            }
        }

    }
}
