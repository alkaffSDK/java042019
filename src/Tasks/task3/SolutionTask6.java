package Tasks.task3;

import java.util.Random;

public class SolutionTask6 {


   public abstract   static class Alien {

        public int health; // 0=dead, 100=full strength
        public String name;

        public Alien(int health, String name) {
            this.health = health;
            this.name = name;
        }

        public abstract int getDamage();

       @Override
       public String toString() {
           return "Alien{" +
                   "health=" + health +
                   ", name='" + name + '\'' +
                   '}';
       }
   }

    public  static class Snack extends Alien {

        public Snack(int health, String name) {
            super(health, name);
        }

        public int getDamage(){
            return  10 ;
        }

        @Override
        public String toString() {
            return "Snack{" +
                    "health=" + health +
                    ", name='" + name + '\'' +
                    '}';
        }
    }

    public  static class Oger extends Alien {

        public Oger(int health, String name) {
            super(health, name);
        }

        public int getDamage(){
            return  6 ;
        }

        @Override
        public String toString() {
            return "Oger{" +
                    "health=" + health +
                    ", name='" + name + '\'' +
                    '}';
        }
    }

    public  static class MarshmallowMan extends Alien {

        public MarshmallowMan(int health, String name) {
            super(health, name);
        }

        public int getDamage(){
            return  1 ;
        }

        @Override
        public String toString() {
            return "MarshmallowMan{" +
                    "health=" + health +
                    ", name='" + name + '\'' +
                    '}';
        }
    }


    public static class AlienPack {
        private Alien[] aliens;

        public AlienPack(int numAliens) {
            aliens = new Alien[numAliens];
        }

        public void addAlien(Alien newAlien, int index) {
            aliens[index] = newAlien;
        }

        public Alien[] getAliens() {
            return aliens;
        }

        public int calculateDamage() {
            int damage = 0;
            for (int i = 0; i < aliens.length; i++) {
                damage += aliens[i].getDamage();
            }
            return damage;
        }

    }

    public static void main(String[] args) {

        Random random = new Random();
       int size =  10 ;
       AlienPack alienPack = new AlienPack(size);
        for(int i=0;i<size;i++ )
        {
            Alien alien = null ;
            switch (random.nextInt(3))
            {
                case 0:
                    alien = new Snack(100,"Snack_"+i);
                    break;
                case 1:
                    alien = new Oger(100,"Oger"+i);
                    break;
                case 2:
                    alien = new MarshmallowMan(100,"MarshmallowMan"+i);
                    break;
            }

            alienPack.addAlien(alien,i);

            System.out.println(alien);
        }

        System.out.println("alienPack.calculateDamage() = " + alienPack.calculateDamage());

        


    }

}
