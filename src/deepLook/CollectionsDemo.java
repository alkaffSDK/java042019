package deepLook;

import java.util.*;

public class CollectionsDemo {

    public static void main(String[] args) {
        System.out.println("============ ArrayList ===========");
        ArrayList<String> arrayList = new ArrayList<>();

        System.out.println("arrayList = " + arrayList);
        arrayList.add("First");
        arrayList.add("Second");
        arrayList.add("Third");
        arrayList.add("Fourth");
        arrayList.add("fifth");
        arrayList.add("tenth");
        arrayList.add("First");
        System.out.println("arrayList = " + arrayList);


        System.out.println("============ Vector ===========");
        Vector<String> vector = new Vector<>();

        System.out.println("vector = " + vector);
        vector.add("First");
        vector.add("Second");
        vector.add("Third");
        vector.add("Fourth");
        vector.add("Fifth");
        vector.add("tenth");
        vector.add("First");
        System.out.println("vector = " + vector);


        System.out.println("============ LinkedList ===========");
        LinkedList<String> linkedList = new LinkedList<>();

        System.out.println("linkedList = " + linkedList);
        linkedList.add("First");
        linkedList.add("Second");
        linkedList.add("Third");
        linkedList.add("Fourth");
        linkedList.add("Fifth");
        linkedList.add("tenth");
        linkedList.add("First");
        System.out.println("linkedList = " + linkedList);

        System.out.println("============ PriorityQueue ===========");


        PriorityQueue<String> priorityQueue = new PriorityQueue<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });
        System.out.println("Fifth".compareTo("First"));
        System.out.println("Fifth".compareTo("Fourth"));
        System.out.println("First".compareTo("Fourth"));
        priorityQueue.add("First");
        System.out.println("priorityQueue = " + priorityQueue);
        priorityQueue.add("Second");
        System.out.println("priorityQueue = " + priorityQueue);
        priorityQueue.add("Third");
        System.out.println("priorityQueue = " + priorityQueue);
        priorityQueue.add("Fourth");
        System.out.println("priorityQueue = " + priorityQueue);
        priorityQueue.add("fifth");
        System.out.println("priorityQueue = " + priorityQueue);
        priorityQueue.add("tenth");
        System.out.println("priorityQueue = " + priorityQueue);
        priorityQueue.add("First");
        System.out.println("priorityQueue = " + priorityQueue);


        System.out.println("============ HashSet ===========");
        HashSet<String> hashSet = new HashSet<>();

        System.out.println("hashSet = " + hashSet);
        hashSet.add("First");
        hashSet.add("Second");
        hashSet.add("Third");
        hashSet.add("Fourth");
        hashSet.add("fifth");
        hashSet.add("tenth");
        hashSet.add("First");
        System.out.println("hashSet = " + hashSet);

        System.out.println("============ LinkedHashSet ===========");
        LinkedHashSet<String> linkedHashSet = new LinkedHashSet<>();

        System.out.println("linkedHashSet = " + linkedHashSet);
        linkedHashSet.add("First");
        linkedHashSet.add("Second");
        linkedHashSet.add("Third");
        linkedHashSet.add("Fourth");
        linkedHashSet.add("Fifth");
        linkedHashSet.add("Tenth");
        linkedHashSet.add("First");
        System.out.println("linkedHashSet = " + linkedHashSet);


        System.out.println("============ TreeSet ===========");
        TreeSet<String> treeSet = new TreeSet<>();

        System.out.println("treeSet = " + treeSet);
        treeSet.add("First");
        treeSet.add("Second");
        treeSet.add("Third");
        treeSet.add("Fourth");
        treeSet.add("Fifth");
        treeSet.add("Tenth");
        treeSet.add("First");
        System.out.println("treeSet = " + treeSet);


        System.out.println("============ HashMap ===========");


        HashMap<String, Object> map = new HashMap<>();
        map.put("Name","Ahmed Alkaff");
        map.put("Email","a.Alkaff@sdkjordan.com");
        map.put("Phone","07889462541");
        map.put("Email2","a.Alkaff@sdkjordan.org");
        System.out.println("map = " + map);

        LinkedList<Map<String, Object>> contacts = new LinkedList<>();

        contacts.add(map);


        HashMap<String, Object> map1 = new HashMap<>();

        map1.put("Name","Mosab Malkawi");
        map1.put("Email","m.malkawi@sdkjordan.com");
        map1.put("Phone","0789888400");

        contacts.add(map1);

        System.out.println("contacts = " + contacts);


        for( Map<String, Object> m : contacts)
        {
            System.out.println("m.keySet() = " + m.keySet());
            System.out.println("m.values() = " + m.values());
            if(m.containsKey("Email2"))
                System.out.println(m.get("Email2"));
        }

        System.out.println("============ TreeMap ===========");

        TreeMap<String, Object> treeMap = new TreeMap<>();
        treeMap.putAll(map);
        System.out.println("treeMap = " + treeMap);

    }
}
