package deepLook;

import basics.Arrays;

import java.lang.reflect.Array;

public class GenericArray {

    static class MyArray<T> {

        private static final int DEFAULT_CAPACITY = 4;
        private T[] array;
        private int size = 0;

        public int getSize() {
            return size;
        }

        public MyArray() {
            this(DEFAULT_CAPACITY);
        }

        public MyArray(int capacity) {
                array = (T[]) new Object[ capacity >0 ? capacity : DEFAULT_CAPACITY];

        }
        public MyArray(int capacity, Class clz ) {
            array = (T[]) Array.newInstance(clz,capacity >0 ? capacity : DEFAULT_CAPACITY);

        }

        public void add(T element) {
            if (isFull())
                expand();

            array[size++] = element;

        }

        public boolean add(int index, T element) {
            if (!isValidIndexForAdd(index))
                return false;         // or  throw  new IndexOutOfBoundsException();

            if (isFull())
                expandWithShift(index, element);
            else {
                int j = size;
                while (j > index) {
                    array[j] = array[j - 1];
                    j--;
                }

                array[index] = element;
            }

            size++;
            return true;
        }

        private void expandWithShift(int index, T element) {
            T[] temp = (T[]) new Object[array.length << 1];
            for (int i = 0; i <= array.length; i++) {
                if (i < index)
                    temp[i] = array[i];
                else if (i == index)
                    temp[i] = element;
                else
                    temp[i] = array[i - 1];
            }

            array = temp;
        }

        private boolean isValidIndexForAdd(int index) {
            return index >= 0 && index <= size;
        }

        private void expand() {
            T[] temp = (T[]) new Object[array.length << 1];             //array.length * 2
            for (int i = 0; i < array.length; i++)
                temp[i] = array[i];

            array = temp;
        }

        private boolean isFull() {
            return size == array.length;
        }

        @Override
        public String toString() {

            if (size == 0 || array == null)
                return "{}";

            StringBuilder builder = new StringBuilder("{");
            for (int i = 0; i < size; i++)
                builder.append(array[i]).append(", ");
            return builder.substring(0, builder.length() - 2) + "}";
        }

        public T getItem(int index) {
            if (index < 0 || index >= size)
                return null;
            return array[index];
        }

        // TODO: implement the following methods

        public T removeFirst() {
            // TODO: remove the first element of the array, and return it
            return null;
        }

        public T removeLast() {
            // TODO: remove the last element of the array, and return it
            return null;
        }

        public boolean remove(T t) {
            // TODO: remove the first occurrence of the  specified element of the array, and return true if it was removed
            return false;
        }

        public T removeAt(int index) {
            // TODO: remove the  element at the specified index of the array, and return it
            return null;
        }

        public int removeAll(T t) {
            // TODO: remove the ALL occurrences of the element in the array, and return the number of removed elements
            return 0;
        }

        public boolean contains(T element) {
            //TODO: check if the array contains the element
            return false;
        }

        public int find(T element) {
            //TODO:  Find the index of the element if exist, return -1 if it not
            return -1;
        }

        public Integer[] findAll(T element) {
            //TODO:  Find all indexes of the element (if exist), return null if it not
            return null;
        }

        public void clear() {
            //TODO:  Remove all elements.
        }

        public void setValueAt(int index, T newElement) {
            //TODO:  change the value of the element in the specified index.
        }
    }

    public static void main(String[] args) {
        MyArray<Integer> integerMyArray = new MyArray<>();
        System.out.println(integerMyArray);

        integerMyArray.add(1);
        System.out.println(integerMyArray);
        integerMyArray.add(2);
        integerMyArray.add(3);
        integerMyArray.add(5);
        integerMyArray.add(7);
        integerMyArray.add(10);
        integerMyArray.add(3, 4);
        System.out.println(integerMyArray);
    }
}
