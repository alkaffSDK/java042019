package deepLook;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;

public class WrapperClasses {

    public static void main(String[] args) {
        int a = 10 ;

        // int -> Integer, double -> Double ....

        // Automatic boxing and unboxing

        a = 10 ;
        Integer i = a ;                     //Automatic  boxing


        Integer ii = new Integer(a) ;       //Automatic  boxing


        int d = i  ;                // Automatic unboxing

        add(a,a);
        add(a,i);
        add(i,a);
        add(i,i);


        a =  100;
        Integer m1 = a ;
        Integer m2 = a ;

        System.out.println(m1 == m2);           // true

        a = 300;
        Integer m3 = a ;
        Integer m4 = a ;

        System.out.println(m3 == m4);           // true



        int  r = Integer.valueOf("011000110010",2) ;// 011000110010
        System.out.println("r = " + r);
        System.out.println("Integer.toBinaryString(r) = " + Integer.toBinaryString(r));
        System.out.println("Integer.toHexString(r) = " + Integer.toHexString(r));
        System.out.println("Integer.toOctalString(r) = " + Integer.toOctalString(r));


        double dd = 50.5 ;
        Double d1 = dd ;
        Double d2 = dd ;
        System.out.println(d1 == d2);
        System.out.println(d1.equals(d2));


        dd = 150.5 ;
         d1 = dd ;
         d2 = dd ;
        System.out.println(d1 == d2);
        System.out.println(d1.equals(d2));

        BigInteger bigInteger = BigInteger.valueOf(21786554848454L);

        BigInteger bigInteger1 = new BigInteger("548822255515515856248756417451456524754561455454854855652485558564546955465");

        System.out.println("bigInteger = " + bigInteger);
        System.out.println("bigInteger1 = " + bigInteger1);

        System.out.println("bigInteger = " + (bigInteger.add(bigInteger1) ));

        System.out.println(Integer.MAX_VALUE);
        Thread t =  new Thread(new Runnable() {
            @Override
            public void run() {
//                StringBuilder builder = new StringBuilder();
//                for (int j = 0; j < Integer.MAX_VALUE - 10; j++) {
//                    builder.append("1");
//                }
                BigInteger x = BigInteger.TWO.pow(Integer.MAX_VALUE-1) ;
                System.out.println("Done");
//                BigInteger c = new BigInteger(builder.toString(),2);
                try(PrintWriter writer = new PrintWriter(new File("data.txt")))
                {
                    System.out.println("Start writhing..");

//                    writer.println("c = " + c);
                    byte[] bytes = x.toByteArray() ;
                    System.out.println("bytes.length = " + bytes.length);
                    for (int j = 0; j < bytes.length; j++) {
                        writer.print(Integer.toBinaryString(bytes[i]));
                    }

                    System.out.println("End writhing..");
                } catch (IOException e) {
                    e.printStackTrace();
                }
               // System.out.println(BigInteger.TWO.pow(Integer.MAX_VALUE-1));
            }
        });

        t.start();

        int counter = 0 ;
        while (t.isAlive())
        {
            try {
                Thread.sleep(10000);
                System.out.print(".");
                if(counter++ % 10 == 0)
                {
                    System.out.print("\r.");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static Integer add(Integer i, Integer j) {
        return  i + j ;
    }

//    public static Integer add(int i, int j) {
//        return  i + j ;
//    }

//    public static Integer add(Integer i, int j) {
//        return  i + j ;
//    }
//
//    public static Integer add(int i, Integer j) {
//        return  i + j ;
//    }
}
