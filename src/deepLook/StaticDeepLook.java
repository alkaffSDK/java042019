package deepLook;

import oop.sharedClasses.Person;

import static java.lang.System.out;

public  class StaticDeepLook {

    // static keyword can be used with
    // 1) instance variables to convert them to be class variables
        // instance variables : are the variables that are exist inside the object, so you have to create an object to use them
        // class variables     : are the variables that are exist inside the class itself,
        // so you do NOT have to create an object to use them, you can  use them using the class name

        // Note: both instance variables and class variables are written inside the class body

    int instanceVariable ;
    static  int staticVariable ;


    // 2) methods : convert the method to class method
    // class method (static method) : are methods exist only inside the class
    // there is only one copy of the static methods
    // to call static methods using class name if it was accessible
    // Static methods are not inheritable

    public static void StaticMethod()
    {
        // can't use the instance variable from a static method, unless you create an object
        // instanceVariable = 10 ;



        StaticDeepLook obj = new StaticDeepLook();
        obj.instanceVariable = 10 ;


        // you can use static variables from any method if it was accessible for it.
        staticVariable = 10 ;
        // OR
        StaticDeepLook.staticVariable = 10 ;
    }


    public void nonStaticMethod()
    {
        // you can use instance variable from noo static methods
        // because this method is exist inside an object , so the instanceVariable will be exist
        instanceVariable = 10 ;


        // you can use static variables from any method if it was accessible for it.
        staticVariable = 10 ;
    }


    // 3) static block

    {
        System.out.println("The is a block of code, this block will be executed every time we create an object of this class");
    }
    // Block are mostly used for variable initializations
    // Example
    static String TempDir ;


    static  {
        System.out.println("The is a static block of code, this block will be executed once at the loading of the class");
        // Create a variable will be cause the class to load .

        TempDir = System.getProperty("java.io.tmpdir") ;
        System.out.println("TempDir = " + TempDir);
    }

    // Note : you can have more than one static or non static block, the compiler will combine them all in one block

    static  {
        System.out.println("The class will be loaded before calling it's method or create an object of it.");
    }
    {
        System.out.println("Another ");
    }
    // 4) inner classes  :
    // inner class : classes defended inside another class or object (outer class)
    // all inner classes are written inside class




    public static void main(String[] args) {


        Person p = new Person() ;
        // can't use the instance variable from a static method, unless you create an object
        // instanceVariable = 10 ;


        // you can use static variables from any method if it was accessible for it.
        staticVariable = 10 ;


        // you can call static variables from any method outside the class if it was accessible for it suing class name
        StaticDeepLook.staticVariable = 10 ;


        // Create object of static inner class

       Outer.StaticInner staticInner = new Outer.StaticInner();


        // Create object of non static inner class
        // In this class you have to create an object of the outer class first
        // because the class is defined inside the object
        // Option 1 :
        Outer outer = new Outer();
        Outer.NonStaticInner nonStaticInner = outer.new NonStaticInner() ;


        // Option 2 :
        Outer.NonStaticInner nonStaticInner1 = new Outer().new NonStaticInner();

        // 5) static import

        System.out.println("Main");

        // by adding import  static java.lang.System.out ;

//        out.println("main");
    }
}
