package deepLook;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;

public class JavaReflectionDemo {

    static class TestClass {
        public int x ;
        public void method()
        {
        }
        public void method1(int a)
        {
        }
    }



    public static void main(String[] args) {
        boolean hasParams = false ;
       Class c =  TestClass.class ;



        for (Method m :c.getMethods())
        {
            hasParams = false ;

            System.out.print(m.getReturnType().getName()+ " "+  m.getName() + "(");
            for (Parameter p : m.getParameters()) {
                System.out.print(p.getType().getName()+ " "+ p.getName() + ", ");
                hasParams = true ;
            }
            System.out.println( hasParams ?"\b\b)": ")");

        }
    }
}
