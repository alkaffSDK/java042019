package deepLook;

public class Outer {

    // Inner and outer classes can access private fields for each other

    private  int Instance ;
    private  static  int Static ;

    public void method()
    {
        StaticInner.StaticInnerStatic = 10 ;

        // StaticInner.StaticInnerInstance = 20 ;

        StaticInner staticInner = new StaticInner();
        staticInner.StaticInnerInstance = 10 ;

        NonStaticInner nonStaticInner = new NonStaticInner();
        nonStaticInner.NonStaticInnerInstance = 10 ;
    }

   public  static class StaticInner {

       private  int StaticInnerInstance ;
       private  static  int StaticInnerStatic ;


       public  void method()
       {
            Static = 10 ;
            //
           // Instance = 10 ;               // need an object
           Outer outer = new Outer();
           outer.Instance = 10 ;
       }

       public static void method1()
       {
           Static = 10 ;
           // static inner class can't access the outer instance members
           // Instance = 10 ;               // need an object
           Outer outer = new Outer();
           outer.Instance = 10 ;
       }

    }

    public class NonStaticInner {
        private  int NonStaticInnerInstance ;
        // Non static inner classes can't have static members
        // private  static  int NonStaticInnerStatic ;



        public  void method()
        {
            Static = 10 ;
            // NON static inner class can access the outer instance members
            Instance = 10 ;               //
            Outer outer = new Outer();
            outer.Instance = 10 ;
        }

    }
}
