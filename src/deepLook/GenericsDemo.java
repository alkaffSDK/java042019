package deepLook;

import java.util.Random;

public class GenericsDemo {

    static class Pair {
        // Key
        private Object key ;
        // Value
        private Object Value ;

        public Object getKey() {
            return key;
        }

        public void setKey(Object key) {
            this.key = key;
        }

        public Object getValue() {
            return Value;
        }

        public void setValue(Object value) {
            Value = value;
        }
    }

    static class PairG<K,V> {
        // Key
        private  K key ;
        // Value
        private V Value ;

        public K getKey() {
            return key;
        }

        public void setKey(K key) {
            this.key = key;
        }

        public V getValue() {

            return Value;
        }

        public void setValue(V value) {
            Value = value;
        }
    }
    public static void main(String[] args) {

        Pair p1 = new  Pair();
        p1.setKey(1);
        p1.setValue("String");


        Pair p2 = new  Pair();
        p2.setKey("String");
        p2.setValue(new Random());




        method(p1);
        method(p2);

        PairG<Integer, String> p3 = new PairG<>();
        p3.setKey(1);
        p3.setValue("string");


        PairG<String, Random> p4 = new PairG<>();
        p4.setKey("String");
        p4.setValue(new Random());

        PairG<Object, Object> p5 = new PairG<>();

        p4.getValue().nextInt();

    }

    public  static  void method(Pair p)
    {
    }
    // accepts any type
    public static  void test(Object o)
    {


    }
    // accepts any type
    public static <T> void testG(T t)
    {


    }
    // accepts any type that extends from String
    public static <T extends String> void testG(T t)
    {
        t.length() ;
    }
    // accepts any type that extends from PairG with String for the Key and any type for the value

    public static <T extends   PairG<String,?>> void testG1(T t)
    {
       t.getKey().length() ;


    }
}
