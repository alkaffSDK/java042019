package oop;

import oop.sharedClasses.Point;

public class ColoredPoint extends Point {

    public static void main(String[] args) {
        Point p = new Point() ;
        // I can see public from anywhere
        p.Public = 1 ;

        // I can not see package (default) from anywhere outside the package
        // p.Package = 10 ;

        // I can not see the protected from anywhere outside the  package and from any child object in a different package
         //  p.Protected = 10 ;

        // Private members are not accessible from  outside the class block
        // p.Private = 21 ;
    }


    public  void method()
    {
        Point p = new Point() ;
        // I can see public from anywhere
        p.Public = 1 ;

        // I can not see package (default) from anywhere outside the package
        // p.Package = 10 ;

        // I can not see the protected from anywhere outside the  package for any object other than the child  object
        // p.Protected = 10 ;          // p is another object, not a parent object for this object
        Protected = 10 ;            // Protected is a member inherited from the parent object

        // Private members are not accessible from  outside the class block
        // p.Private = 21 ;
    }

}
