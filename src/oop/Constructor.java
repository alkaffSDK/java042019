package oop;

public class Constructor {

    // Constructor : a spacial method has the same name as the class, with no return data type
    // Each and every class must have a constructor
    // The compiler will create a default empty constructor, if and if the developer did not create one
    // Constructor can be overloaded, but overridden
    // The first statement at any constructor body is a call to another constructor
    //  this call can be either
    //  a)  a call to constructor in the parent class using super()
    //  b)  a call to constructor in the same class using this()
    // In constructor if the developer did not call another constructor at the first statement, then the compiler will call super()
    // The constructor is the first method to be called AFTER creating the object

    static class  A {

        public static int _id = 0 ;

        public  int id  ;
        public  int m  ;
       public  A (int a)
       {

           super();                     // this is a call for the constrictor at the parent class
           id  =  _id++ ;
           System.out.println("A(int) :"+ id);

       }

        public  A()
        {
            this(0);        // this will call A (int a)
            System.out.println("A() :"+ id);
        }

        @Override
        protected void finalize() throws Throwable {
            super.finalize();
            System.out.println("The object will destroyed! id:"+id);
        }
    }

    static class  B extends  A {

        public  B(){
            //this(0);
            System.out.println("B()");
        }

        public  B(int a){
           // super(a);
            System.out.println("B(int)");
        }
    }
    public static void main(String[] args) {

//        A a = new A();                      // call the constructor A()
//        A a1 = new A(10);                // call the constructor A(int a)

        System.out.println("=====new A()=======");
        new A();
        System.out.println("=====new A(10)=======");
        new A(10);
        System.out.println("=====new B()=======");
        new B();
        System.out.println("=====new B(10)=======");
        new B(10);

        Holder h = new Holder() ;
        Text(h) ;
        System.out.println(h.a.m);

    }


    public static void  Text(Holder h) {

        for (int i = 0; i < 5 ; i++) {
            h.a = new A();
        }
        System.gc();
        h.a.m = 20 ;
    }

    static  class Holder {
        A a ;
    }
}
