package oop;

import oop.sharedClasses.Point;
import oop.sharedClasses.User;

public class ObjectClassMethods {

    public static void main(String[] args) {

        Point p = new Point();
        p.x = 1 ;
        p.y = 2 ;
        p.z = 3 ;

        Point p1 = new Point();
        p1.x = 1 ;
        p1.y = 2 ;
        p1.z = 3 ;

        System.out.println(p == p1);

        System.out.println(p.equals(p1));

        User user = new User();
        System.out.println(p.equals(user));

        System.out.println("p = " + p);         // call p.toString()
        System.out.println("Integer.toHexString(p.hashCode()) = " + Integer.toHexString(p.hashCode()));
    }
}
