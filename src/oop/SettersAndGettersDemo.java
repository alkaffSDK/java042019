package oop;

import oop.sharedClasses.User;

public class SettersAndGettersDemo {

    static class Account {
        private String name ;
        private double value ;
        private int id ;
        private String password ;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getValue() {
            return value;
        }

        public void setValue(double value) {
            this.value = value;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }
    public static void main(String[] args) {


        Account a = new Account();
        a.id =1;
        a.name = "Ahmed";
        a.value = -200.5;
        a.password = "123";
    }
}
