package oop.sharedClasses;

public class User {
    private   int Age;
    private String Name;
    private String Password;
    private String Login ;

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        if(null != Name  && Name.length()>= 3)
            this.Name = Name;
        else
            this.Name = "No name";
    }

    public boolean checkPassword(String pass) {
        return Password.equals(pass);
    }

    public boolean changePassword(String password) {
        if(null != password && password.length() >= 6) {
            Password = password;
            return  true ;
        }

        return  false ;
    }

    public String getLogin() {
        return Login;
    }

    public void setLogin(String login) {
        Login = login;
    }

    public  void setAge(int a)
    {
        if(a > 0 && a < 150)
            Age = a ;

    }

    public  int getAge() {return  Age ;}


    @Override
    public String toString() {
        return "User{" +
                "Age=" + Age +
                ", Name='" + Name + '\'' +
                ", password='" + Password + '\'' +
                ", Login='" + Login + '\'' +
                '}';
    }
}
