package oop.sharedClasses;

public class Person {

    static  {
        System.out.println("Person static block" );
    }
    protected int Id;
    protected String Name ;
    protected Gender gender ;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void print()
    {
        System.out.println("Person");
    }
    public  void MethodInPerson()
    {

    }

    @Override
    public String toString() {
        return "Person{" +
                "Id=" + Id +
                ", Name='" + Name + '\'' +
                ", gender=" + gender +
                '}';
    }
}
