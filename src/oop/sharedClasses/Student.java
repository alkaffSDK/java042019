package oop.sharedClasses;

public class Student extends Person {
    public String Major ;
    public  double Agp ;

    int Id;
    public  void MethodInStudent()
    {
        // if there is an Id variable inside Student
        Id = 10 ;       // Student Id
        super.Id = 20 ; // Person Id


        // if there is no Name variable inside Student
        Name = "Ahmed" ;               //  Person Name
        super.Name = "Ahmed" ;          //  Person Name


    }
    public void print()
    {
        System.out.println("Student");
    }

    @Override
    public String toString() {
        return "Student{" +
                "Major='" + Major + '\'' +
                ", Agp=" + Agp +
                ", Id=" + getId() +
                ", Name='" + getName() + '\'' +
                ", gender=" + getGender() +
                '}';
    }
}
