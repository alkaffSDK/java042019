package oop.sharedClasses;

public class Point {
    public int x
            , y , z ;

    public  int Public ;            // any where
    int Package ;                   // any where
    protected   int Protected ;     // any where
    private   int Private ;         // any where


    int d = Private ;

    public Point(int x) {
        this(x, 0, 0);
    }

    public Point(int x, int y) {
       this(x,y,0);
    }
    public Point(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Point() {

    }

    public  void method()
    {
        System.out.println("Private = " + Private);
        System.out.println("Package = " + Package);
        System.out.println("Protected = " + Protected);
        System.out.println("Public = " + Public);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof  Point) {
            Point p = (Point) obj;
            return x == p.x && y == p.y && z == p.z;
        }
        return  false ;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}
