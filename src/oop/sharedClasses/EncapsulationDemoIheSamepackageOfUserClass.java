package oop.sharedClasses;

public class EncapsulationDemoIheSamepackageOfUserClass {
    public static void main(String[] args) {


        Point p = new Point() ;
        // I can see public from anywhere
        p.Public = 1 ;

        // I can see package (default) from anywhere inside the same package
        p.Package = 10 ;

        // I can see the protected from anywhere inside the same package and from any child object in a different package
        p.Protected = 10 ;

        // Private members are not accessible from  outside the class block
        // p.Private = 21 ;

    }
}
