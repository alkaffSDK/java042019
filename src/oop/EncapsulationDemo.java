package oop;


import oop.sharedClasses.Point;
import oop.sharedClasses.User;

public class EncapsulationDemo {
    public static void main(String[] args) {

        Point p = new Point() ;
        // I can see public from anywhere
        p.Public = 1 ;

        // I can not see package (default) from anywhere outside the package
        // p.Package = 10 ;

        // I can not see the protected from anywhere outside the  package and from any child object in a different package
        // p.Protected = 10 ;

        // Private members are not accessible from  outside the class block
        // p.Private = 21 ;


    }
}
