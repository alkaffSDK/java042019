package oop;

import oop.sharedClasses.Point;

public class InheritanceDemo {


    // In java : only single inheritance is allowed !!
    // There is only one and only one parent class for any class in Java (except for Object class)
    static class Parent extends Point {
        // static member are no inheritable

        int x , y  ;

        public Parent() {
            super(0,0,0);
        }
        public Parent(int x) {
            super(x);
        }

        public Parent(int x, int y) {
            super(x, y);
        }

        public Parent(int x, int y, int z) {
            super(x, y, z);
        }

        public  void method()
        {
            x = 10 ;                 // this x is the local member in this class
            super.x = 10 ;          // this x of the parent object (of type Point)
            z = 10 ;
        }
        public static void staticMethod() {
            // Error  Protected = 10; //


        }

        // non static members are inheritable but not all of them are accessible  ( private and ( package for class in different packages ) are not )
        protected void nonStatic() {
            Public = 10;
            Protected = 10;
        }
    }

    static class Child extends Parent {

    }

    public static void main(String[] args) {

        Parent p = new Parent();
        Child c = new Child();
        Object d ;

    }
}
