package oop;

import oop.sharedClasses.Employee;
import oop.sharedClasses.Person;
import oop.sharedClasses.Student;

public class PolymorphismDemo {
    // Polymorphism types

    //  1) static Polymorphism (not important in Java)

    //          a) Method overloading : defining more than one method using the the same name but different parameter list
    public static int add(int a) {
        return a;
    }

    public static int add(int a, int b) {
        return a + b;
    }

    public static double add(double a) {
        return a;
    }
    //          b) Operator overloading (not exist in Java)


    // 2) Dynamic Polymorphism

    //          a) Parent referencing



    //          b) Late binding


    public static void main(String[] args) {

        // Method overloading
        int m = 10, n = 20;
        double d = 15.32;
        add(d);


        // Parent referencing
//
        // super type variable can refers to any sub type object
        Person p =  new Person();
        Person ps = new Student();
        Person pe = new Employee();


        //  Late binding : calling the the same method using the same variable type but getting different result
        p.print();         // Person
        ps.print();        // Student
        pe.print();        // Employee


        Student s = new Student();
        //Student sh  = new Human();        // subclass variable  can NOT refers to super class object
        // sh.StudentID = 10 ;

        Employee e = new Employee();
        // Employee eh = new Human();    // subclass variable  can NOT refers to super class object

    }
}
