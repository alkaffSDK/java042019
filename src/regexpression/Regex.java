package regexpression;

import java.io.*;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex {

    public static void main(String[] args) {

        // ( ) is a group

        String regex = "([A-Za-z_][\\w-._]*)@(\\w*\\.([[\\w.]{2,10}]+))";
        String line = "";
        Pattern pattern = Pattern.compile(regex);

        Matcher matcher;
        try (BufferedReader reader = new BufferedReader(new FileReader(new File("bad emals.txt")))) {

//            reader.lines().forEach((s)-> System.out.println(s));
            while ((line = reader.readLine()) != null) {
                matcher = pattern.matcher(line);
                StringBuffer stringBuffer = new StringBuffer("{");
                if (matcher.find()) {
                    System.out.println("Line :" + line);
                    for (int i = 0; i <= matcher.groupCount(); i++) {
                        System.out.printf(Locale.getDefault(),"Group %3d :%-50s%10d, %10d%n", i, matcher.group(i), matcher.start(i), matcher.end(i));
                    }
                    // System.out.println(matcher.group(0));
                }

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
