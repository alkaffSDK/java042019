package networking;

public class Constants {
    public static final String SERVER_ADDRESS = "127.0.0.1";
    public static final int SERVER_PORT = 9875;
}
