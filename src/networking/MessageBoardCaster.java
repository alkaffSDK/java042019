package networking;

import java.util.ArrayList;
import java.util.Scanner;

public class MessageBoardCaster extends Thread {
    ArrayList<ClientsWriter> mClientsWriters ;
    public MessageBoardCaster(ArrayList<ClientsWriter> clientsWriters) {
        mClientsWriters = clientsWriters ;
        start();
    }


    @Override
    public void run() {
        Scanner scanner = new Scanner(System.in);

      while (true)
      {
          try {

              while (true)
              {
                  System.out.println("Server :");
                  String msg = scanner.nextLine();

                  mClientsWriters.forEach(r ->r.sendMessage(msg));
              }
          }catch (Exception ex)
          {
              break;
          }

      }
    }
}
