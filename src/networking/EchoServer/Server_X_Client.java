package networking.EchoServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;


public class Server_X_Client {
    public static void main(String args[]) {


        Socket s = null;
        ServerSocket ss2 = null;
        System.out.println("Server Listening......");
        try {
            ss2 = new ServerSocket(4445);

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Server error");

        }

        while (true) {
            try {
                s = ss2.accept();
                System.out.println("connection Established");
                ServerThread st = new ServerThread(s);
                st.start();

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Connection Error");

            }
        }

    }

}

class ServerThread extends Thread {

    String line = null;
    BufferedReader bufferedReader = null;
    PrintWriter printWriter = null;
    Socket mClient = null;

    public ServerThread(Socket client) {
        this.mClient = client;
    }

    public void run() {
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(mClient.getInputStream()));
            printWriter = new PrintWriter(mClient.getOutputStream());

        } catch (IOException e) {
            System.out.println("IO error in server thread");
        }

        try {
            line = bufferedReader.readLine();            // read from client
            while (line.compareTo("QUIT") != 0 && !mClient.isClosed()) {

                printWriter.println("\'" + line + "\'");            // write to client
                printWriter.flush();
                System.out.println("Response to Client  :  " + line);
                line = bufferedReader.readLine();            // read from client again
            }
        } catch (IOException e) {

            line = this.getName(); //reused String line for getting thread name
            System.out.println("IO Error/ Client " + line + " terminated abruptly");
        } catch (NullPointerException e) {
            line = this.getName(); //reused String line for getting thread name
            System.out.println("Client " + line + " Closed");
        } finally {
            try {
                System.out.println("Connection Closing..");
                if (bufferedReader != null) {
                    bufferedReader.close();
                    System.out.println(" Socket Input Stream Closed");
                }

                if (printWriter != null) {
                    printWriter.close();
                    System.out.println("Socket Out Closed");
                }
                if (mClient != null) {
                    mClient.close();
                    System.out.println("Socket Closed");
                }

            } catch (IOException ie) {
                System.out.println("Socket Close Error");
            }
        }//end finally
    }
}