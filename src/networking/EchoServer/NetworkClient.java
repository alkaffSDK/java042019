package networking.EchoServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

public class NetworkClient {

	public static void main(String args[]) throws IOException {

		InetAddress address = InetAddress.getLocalHost();
		Socket s1 = null;
		String line = null;
		BufferedReader readfromUser = null;
		BufferedReader readFromServer = null;
		PrintWriter writeToserver = null;

		try {
			s1 = new Socket(address, 4445); // You can use static final constant
											// PORT_NUM
			readfromUser = new BufferedReader(new InputStreamReader(System.in));            // can replace this with  Scanner
			readFromServer = new BufferedReader(new InputStreamReader(
					s1.getInputStream()));
			writeToserver = new PrintWriter(s1.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
			System.err.print("IO Exception");
		}

		System.out.println("Client Address : " + address);
		System.out.println("Enter Data to echo Server ( Enter QUIT to end):");

		String response = null;
		try {
			line = readfromUser.readLine();
			while (line.compareTo("QUIT") != 0) {
				writeToserver.println(line);
				writeToserver.flush();
				response = readFromServer.readLine();
				System.out.println("Server Response : " + response);
				line = readfromUser.readLine();
			}

		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Socket read Error");
		} finally {

			readFromServer.close();
			writeToserver.close();
			readfromUser.close();
			s1.close();
			System.out.println("Connection Closed");

		}

	}
}