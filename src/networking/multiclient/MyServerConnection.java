package networking.multiclient;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class MyServerConnection extends Thread {
	private static final long WAITING_DELAY = 500;
	private Server mServer;
	private Socket mClient;
	private String Name;
	

	private DataInputStream mDataInputStream;
	private DataOutputStream mDataOutputStream;
	private boolean Running = true;

	public MyServerConnection(String name, Server server, Socket client) {
		super(name);
		Name = name;
		mServer = server;
		mClient = client;
	}

	private void SentMessageToClient(String text) {
		try {
			mDataOutputStream.writeUTF(text);
			mDataOutputStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void SentMessageToAllClient(String text) {
		for (MyServerConnection s : mServer.Clients) {
			 if(!s.equals(this))
				 s.SentMessageToClient(text);
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof MyServerConnection) {
			return ((MyServerConnection) obj).getName().equalsIgnoreCase(Name);
		}
		return false;
	}

	@Override
	public void run() {
		try {
			mDataInputStream = new DataInputStream(mClient.getInputStream());
			mDataOutputStream = new DataOutputStream(mClient.getOutputStream());

			//SentMessageToClient("Hello");
			while (Running) {
				try {
					while (mDataInputStream.available()== 0) {
						//System.out.println("waiting:"+mDIS.available());
						try {
							Thread.sleep(WAITING_DELAY);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					String data = mDataInputStream.readUTF();
					System.out.println("Server Received:"+data);
					//SentMessageToAllClient(Name+":"+data);
				} catch (IOException e) {
					e.printStackTrace();
				}				
				// System.out.println(Name+":"+data);
			}	
			close();


		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void close() {
		try {
			mClient.close();
			mDataOutputStream.close();
			mDataInputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
