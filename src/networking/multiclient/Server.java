package networking.multiclient;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {

	public static final int PORT = 9938;

	ServerSocket mServerSocket = null;
	

	private static int ClientCounter = 0;

	ArrayList<MyServerConnection> Clients = new ArrayList<>();

	private boolean Running = true;

	public Server(int port) {
		try {
			mServerSocket = new ServerSocket(port);

			while (Running) {
				System.out.println("Waiting for a client to connect");
				Socket clientSocket = mServerSocket.accept();
				MyServerConnection serverConnection = new MyServerConnection("Client"
						+ ++ClientCounter, this, clientSocket);
				serverConnection.start();
				Clients.add(serverConnection);
				System.out.println(serverConnection.getName()
						+ " is connected to the server.");
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			System.out.println("Finsh the connection");
		}
	}

	public static void main(String[] args) {
		new Server(PORT);
	}
}
