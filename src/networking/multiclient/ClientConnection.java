package networking.multiclient;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientConnection extends Thread {

	private static final long WAITING_DELAY = 10;
	private Socket mServer;
	private String Name;

	private DataInputStream mDIS;
	private DataOutputStream mDOS;
	private boolean Running = true;

	public ClientConnection(String name, Socket server) {
		super(name);
		Name = name;
		mServer = server;
	}

	public void SentMessageToServer(String text) {
		try {
			mDOS.writeUTF(text);
			mDOS.flush();
		} catch (IOException e) {
			e.printStackTrace();
			// close();
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ClientConnection) {
			return ((ClientConnection) obj).Name.equalsIgnoreCase(Name);
		}
		return false;
	}

	@Override
	public void run() {
		try {
			mDIS = new DataInputStream(mServer.getInputStream());
			mDOS = new DataOutputStream(mServer.getOutputStream());

			while (Running) {

				try {
					while (mDIS.available() == 0) {
						try {
							Thread.sleep(WAITING_DELAY);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					String data = mDIS.readUTF();
					SentMessageToServer(data);
					System.out.println(data);
				} catch (IOException e) {
					e.printStackTrace();
					close();

				}
			}

		} catch (IOException e) {
			e.printStackTrace();
			close();
		}
	}

	void close() {
		try {
			mDOS.close();
			mDIS.close();
			mServer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
