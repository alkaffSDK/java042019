package networking.SocketServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ReaderThread extends Thread {

    private InputStream inputStream ;
    private String name ;
    public ReaderThread(InputStream inputStream, String name) {
        this.inputStream = inputStream ;
        this.name = name ;

    }

    @Override
    public void run() {
        String line = "";

        try {
            if(null != inputStream)
            {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream)) ;
                while (null != (line =reader.readLine())) {
                    System.out.println(name + " :" + line);
                    if(line.equalsIgnoreCase("bye"))
                        break;;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
