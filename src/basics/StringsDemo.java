package basics;

public class StringsDemo {

    // String : is array of characters
    public static void main(String[] args) {

        int x = 10 ;

        Object o = new Object();

        String str = "String";
        String str1 = "String";
        String string = new String("String") ;
        String string2 = new String("String") ;

        System.out.println("str     :"+ str);
        System.out.println("string  :"+ string);

        System.out.println("(str == str1) = " + (str == str1));             // 1) true , false
        System.out.println("(string == string2) = " + (string == string2)); // 2) true , false
        System.out.println("(str == string) = " + (str == string));         // 3) true , false

        System.out.println("----------------------------------");
        System.out.println("(str.equals(str1)) = " + (str.equals(str1)));
        System.out.println("(string.equals(string2)) = " + (string.equals(string2)));
        System.out.println("(str.equals(string)) = " + (str.equals(string)));
        System.out.println("----------------------------------");

        str = "string" ;
        System.out.println("(str.equals(str1)) = " + (str.equals(str1)));
        System.out.println("(string.equals(string2)) = " + (string.equals(string2)));
        System.out.println("(str.equals(string)) = " + (str.equals(string)));
        System.out.println("----------------------------------");
        System.out.println("(str.equalsIgnoreCase(str1)) = " + (str.equalsIgnoreCase(str1)));
        System.out.println("(string.equalsIgnoreCase(string2)) = " + (string.equalsIgnoreCase(string2)));
        System.out.println("(str.equalsIgnoreCase(string)) = " + (str.equalsIgnoreCase(string)));
        System.out.println("----------------------------------");



        String newString = str.replaceAll("t", "e");
        System.out.println("str = " + str);
        System.out.println("newString = " + newString);

        str.charAt(0);
        System.out.println("string".compareTo("string"));
        System.out.println("string".compareTo("String"));
        System.out.println("String".compareTo("string"));
        System.out.println("string ".compareTo("string"));
        System.out.println("string".compareTo("string "));


        int [] arr = {1,2,4,5,6,7,8,9,20};

        String s = "{";
        for(int i=0;i<arr.length;i++)
            if(i != arr.length -1 )
            s += arr[i] + "," ;
        else
            s += arr[i] + "}" ;
        System.out.println("s = " + s);

        StringBuilder stringBuilder = new StringBuilder();
        StringBuffer sb = new StringBuffer("{");
        for(int i=0;i<arr.length;i++)
            if(i != arr.length -1 )
                sb.append(arr[i]).append(", ") ;
            else
                sb.append(arr[i]).append("}") ;

        s= sb.toString();
        System.out.println("sb = " + sb);
        System.out.println("s = " + s);

        // s = {
        // s = {1,
        // s = {1,2,
        // s = {1,2,4,

    }
}
