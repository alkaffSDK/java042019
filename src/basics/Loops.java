package basics;

import java.util.Locale;

public class Loops {

    public static void main(String[] args) {

//       int x  = 2 , d  = 2;
//        for (   x = 0  , d = 0, System.out.println("A"),System.out.println("A1") , System.out.println("A2")   ;
//                x < 10;
//                System.out.println("C"),System.out.println("C1") , System.out.println("C2"))
//            System.out.println("x++ = " + x++);

        short a = 5;

        a = 0;
        while (a < 10) {
            System.out.println("a = " + ++a);
            if(a % 3  == 0)
                continue;

            System.out.println("After break/continue");
        }
        System.out.println("Finally a = " + a);


        System.out.println("============= For loop =============");
        for(System.out.println("A"), a = 0; a< 5 ; System.out.println("C"),a++)
        {
            System.out.printf(Locale.getDefault(),"%5d -> Statement or block {}\t",a);

        }

        System.out.println("============= Do/While loop =============");

        a = 5 ;
        do {
            System.out.println("a = " + a);
            a++ ;
        }
        while(a <10);

////        for(System.out.print("start"), System.out.print("...."), a= 2 , c=3;a  > 10; System.out.println("C"),a++)
////            System.out.println("statement");
//
////        for (int c = -a; c <= a; c++)
////            if ((c & 1) == 0)
////                System.out.println("c+  = " + c + " Yes");
////            else
////                System.out.println("c+  = " + c + " No");
//

        // break statement stop the loop
        // continue stop the current loop round and continue from the start of the loop


//            for(int i=0;i<10;i++ )
//                System.out.println("i = " +  i);

        short i = 0, j;
        for (; i < 10; i++) {
            j = i;
            while (j > 0) {
                if ((j-- & 1) == 0)
                    continue;
                System.out.println("I = " + i + ", J= " + j);
            }
        }
        System.out.println("Finally i = " + i);
    }
}
