package basics;

import java.util.Locale;

public class PrintFormatDemo {

    public static void main(String[] args) {
        System.out.println("Println");
        System.out.print("Print");
        System.out.println();

        double gold = 1542.25 ;
        double silver = 19.33 ;
        double platinum = 102.65 ;

        int year = 2019 ;

        System.out.println("The Gold prince in 2019 is :"+gold + "USD");
        System.out.println("The Silver prince in 2019 is :"+silver + "USD");
        System.out.println("The Platinum prince in 2019 is :"+platinum + "USD");

        System.out.println("--------------------------------");
        System.out.printf(Locale.getDefault(), "The %s prince in %d is :%f %n","Gold",year,gold);
        System.out.printf(Locale.getDefault(),"The %s prince in %d is :%f %n","Silver",year,silver);
        System.out.printf(Locale.getDefault(),"The %s prince in %d is :%f %n","Platinum",year,platinum);

        System.out.println("--------------------------------");
        System.out.printf(Locale.getDefault(),"The %-10s prince in %05d is :%10.2f %n","Gold",year,gold);
        System.out.printf(Locale.getDefault(),"The %-10s prince in %05d is :%10.2f %n","Silver",year,silver);
        System.out.printf(Locale.getDefault(),"The %-10s prince in %05d is :%10.2f %n","Platinum",year,platinum);



    }
}
