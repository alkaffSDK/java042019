package basics;


import java.math.BigInteger;
import java.util.Scanner;

public class ConsoleInput {
    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);
        int a =10;
        System.out.print("Please enter the value of A: ");
        a = scanner.nextInt() ;         // cin<<a ;
        System.out.println("a = " + a);
        BigInteger bigInteger = new BigInteger("56551455548785552458556478556545548");
        System.out.println("bigInteger = " + bigInteger);
        System.out.print("Please enter the value of BigInteger: ");

        BigInteger v = scanner.nextBigInteger() ;
        System.out.println("v = " + v);

        BigInteger r = BigInteger.TWO.pow(Integer.MAX_VALUE - 1);

        System.out.println("Done");
        System.out.println("r = " + r);
        System.out.println("New");

        System.out.println("Another");
        
    }
}
