package basics;

import java.util.Locale;

public class Operators {

    public static void main(String[] args) {
        /**
          + , - ,*, /, % , < , >, <=, >= , =, == , != , ++ , -- , (-) , +=, -= , &&, ||,!


         *  & (BITWISE AND),
         *  | BITWISE OR),
         *  ^ BITWISE XOR),
         *  << (left SHIFT),
         *  >> (RIGHT SHIFT),
         *  >>> (zero filled right shift),
         *  ~ (complement),
         */


        int a = 10, b = 3;

        // Addition (+)
        System.out.println(10 + 3.5 + 2);                // 15.5
        System.out.println("10" + 3 + 2);              // 1032,
        System.out.println(10 + "3" + 2);              // 1032
        System.out.println(10 + 3 + "2");              // 132

        System.out.println("a + b =" + a + b);          // a + b =103
        System.out.println("a + b =" + (a + b));          // a + b =13

        System.out.println("5.3 + 5 =" + (5.3 + 5));        // 10.3
        System.out.println("'A' + 'B' =" + ('A' + 'B'));    // 131
        System.out.println("'A' + 'B' =" + 'A' + 'B');      // 'A' + 'B' =AB
        System.out.println('A' + 'B');    // 131



        a = 15;
        System.out.println(a++);         // 15 then add 1 to a to a be 16
        System.out.println(a);           // 16
        System.out.println(++a);         // add 1 to a (was 16) to be 17 then print the new value 17

        a  = 17; b = 3 ;
        System.out.println(a == b);     // means is a equals to b (true/false)
        System.out.println(a != b);
        System.out.println(a > b);
        System.out.println(a < b);
        System.out.println(a <= b);
        System.out.println(a >= b);

       //  System.out.println( a && b );          // Error: because && , || , ! accepts boolean operands only
        System.out.println(a > 10 && b > 10);    //

        System.out.println(!(false && true));

        System.out.println("a % b =" + (a % b));            // 1
        System.out.println("10.5 % b =" + (10.5 % b));      // 1.5
        System.out.println("10.5 % 3.5 =" + (10.5 % 3.5));  // 0

        System.out.printf(Locale.getDefault(), "%d & %d = %d%n", a, b, (a & b));        //
            // 10001
            // 00011
        //------------
            // 00001
        System.out.println("A:" + a + ", B:" + b);
        System.out.printf(Locale.getDefault(),"A:%d , B:%d%n",a,b );

        a = 10;
        b = 3;
        System.out.printf(Locale.getDefault(), "%d & %d = %d%n", a, b, (a & b));       // 2
        // 1010
        // 0011
        //----- AND
        // 0010
        System.out.printf(Locale.getDefault(), "%d | %d = %d%n", a, b, (a | b));       // 11
        // 1010
        // 0011
        //----- OR
        // 1011
        System.out.printf(Locale.getDefault(), "%d ^ %d = %d%n", a, b, (a ^ b));        // 9
        // 1010
        // 0011
        //----- XOR
        // 1001

        int r = 10;
        r *= b + 2;       // r  = r* (b+ 2)
        System.out.println("R: " + r);      // 50

        a = 2;
        b = 3;

        int c = 3 * a - b * 2 * (a + b) / 3;
        // c = -4;
        System.out.println("First c :" + c);

        c = 3 * a++ - b++ * 2 * (a++ + b++) / 3;
        // a = 2  , b = 3
        //  c = 3 * a++ - b++ * 2 * (a++ + b++) / 3;
        System.out.println("Second c :" + c);


        a = 10;         //  00000000001010
        b = 3;         //

        //
        Locale locale = Locale.getDefault();
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a", a, String.format(locale, "%32s", Integer.toBinaryString(a)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "b", b, String.format(locale, "%32s", Integer.toBinaryString(b)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a & b", a & b, String.format(locale, "%32s", Integer.toBinaryString(a & b)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a | b", a | b, String.format(locale, "%32s", Integer.toBinaryString(a | b)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a ^ b", a ^ b, String.format(locale, "%32s", Integer.toBinaryString(a ^ b)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "~a", ~a, String.format(locale, "%32s", Integer.toBinaryString(~a)).replaceAll(" ", "0"));

        System.out.println("\n                     positive value                             \n");
        System.out.println("----------- shift left  <<   ---------------");
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a << 0", a << 0, String.format(locale, "%32s", Integer.toBinaryString(a << 0)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a << 1", a << 1, String.format(locale, "%32s", Integer.toBinaryString(a << 1)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a << 2", a << 2, String.format(locale, "%32s", Integer.toBinaryString(a << 2)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a << 3", a << 3, String.format(locale, "%32s", Integer.toBinaryString(a << 3)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a << 4", a << 4, String.format(locale, "%32s", Integer.toBinaryString(a << 4)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a << 26", a << 26, String.format(locale, "%32s", Integer.toBinaryString(a << 26)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a << 27", a << 27, String.format(locale, "%32s", Integer.toBinaryString(a << 27)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a << 28", a << 28, String.format(locale, "%32s", Integer.toBinaryString(a << 28)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a << 29", a << 29, String.format(locale, "%32s", Integer.toBinaryString(a << 29)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a << 30", a << 30, String.format(locale, "%32s", Integer.toBinaryString(a << 30)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a << 31", a << 31, String.format(locale, "%32s", Integer.toBinaryString(a << 31)).replaceAll(" ", "0"));



        System.out.println("-----------  shift right >>   ---------------");
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >> 0", a >> 0, String.format(locale, "%32s", Integer.toBinaryString(a >> 0)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >> 1", a >> 1, String.format(locale, "%32s", Integer.toBinaryString(a >> 1)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >> 2", a >> 2, String.format(locale, "%32s", Integer.toBinaryString(a >> 2)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >> 3", a >> 3, String.format(locale, "%32s", Integer.toBinaryString(a >> 3)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >> 4", a >> 4, String.format(locale, "%32s", Integer.toBinaryString(a >> 4)).replaceAll(" ", "0"));

        System.out.println("-----------  zero filled shift right >>>   ---------------");
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >>> 0", a >>> 0, String.format(locale, "%32s", Integer.toBinaryString(a >>> 0)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >>> 1", a >>> 1, String.format(locale, "%32s", Integer.toBinaryString(a >>> 1)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >>> 2", a >>> 2, String.format(locale, "%32s", Integer.toBinaryString(a >>> 2)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >>> 3", a >>> 3, String.format(locale, "%32s", Integer.toBinaryString(a >>> 3)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >>> 4", a >>> 4, String.format(locale, "%32s", Integer.toBinaryString(a >>> 4)).replaceAll(" ", "0"));


        a= -10 ;
        System.out.println("\n                     Negative value                             \n");
        System.out.println("----------- shift left  <<   ---------------");
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a << 0", a << 0, String.format(locale, "%32s", Integer.toBinaryString(a << 0)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a << 1", a << 1, String.format(locale, "%32s", Integer.toBinaryString(a << 1)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a << 2", a << 2, String.format(locale, "%32s", Integer.toBinaryString(a << 2)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a << 3", a << 3, String.format(locale, "%32s", Integer.toBinaryString(a << 3)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a << 4", a << 4, String.format(locale, "%32s", Integer.toBinaryString(a << 4)).replaceAll(" ", "0"));

        System.out.println("-----------  shift right >>   ---------------");
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >> 0", a >> 0, String.format(locale, "%32s", Integer.toBinaryString(a >> 0)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >> 1", a >> 1, String.format(locale, "%32s", Integer.toBinaryString(a >> 1)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >> 2", a >> 2, String.format(locale, "%32s", Integer.toBinaryString(a >> 2)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >> 3", a >> 3, String.format(locale, "%32s", Integer.toBinaryString(a >> 3)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >> 4", a >> 4, String.format(locale, "%32s", Integer.toBinaryString(a >> 4)).replaceAll(" ", "0"));

        System.out.println("-----------  zero filled shift right >>>   ---------------");
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >>> 0", a >>> 0, String.format(locale, "%32s", Integer.toBinaryString(a >>> 0)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >>> 1", a >>> 1, String.format(locale, "%32s", Integer.toBinaryString(a >>> 1)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >>> 2", a >>> 2, String.format(locale, "%32s", Integer.toBinaryString(a >>> 2)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >>> 3", a >>> 3, String.format(locale, "%32s", Integer.toBinaryString(a >>> 3)).replaceAll(" ", "0"));
        System.out.printf(locale, "%-10s :%11d -->%s%n", "a >>> 4", a >>> 4, String.format(locale, "%32s", Integer.toBinaryString(a >>> 4)).replaceAll(" ", "0"));



        a = 8;
        System.out.println(a < 0 && a++ != 10);         // false
        System.out.println(a);                           // 8 , because with logical operator (&&,||)  uses the short circuit

        a = 8;
        System.out.println(a < 0 & a++ != 10);          // false
        System.out.println(a);                          // 9

    }
}
