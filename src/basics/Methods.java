package basics;

import java.util.Random;

public class Methods {

    // Method : named block of code with an output and [optional] inputs

    // Syntax: to define a method : [modifier] [specifier] <return_data_type> <method_name> ([<Variable>]* ) { [body]}

    // Syntax: to call a method  : [ClassName.|Variable.]<methodName>([arguments]) ;

    // Notes: All method in Java, must be *WRITTEN* directly inside a class or a type.
    //          return statement must be exist at the end of every path inside any method with non void return data type
    {
        // this is a block (unnamed block)
    }
    void name() {
        // this is a block
        return ; // this will end the method execute here ;

        // System.out.println("This is not reachable");
        // return 0 ;
    }

    void method1(int a) {       // a = 10
        // this is a block
        name();
    }

    void mathod2(int a, double b) {
        // this is a block

        method1(10);

        int c = 20 ;
        method1(c);

        int m = add(10,c) ;         // m = 30

    }

    int add(int a, int b)
    {
        if(a > b)
        return  a+ b ;
        else return  a-b ;
    }

    static void staticDoSome() {
        System.out.println("staticDoSome()");

        // anotherSome();
    }

    void doSome() {
        System.out.println("doSome()");
        staticDoSome();


    }


    void anotherSome() {
        doSome();

        staticDoSome();
    }


    void sayHi() {
        System.out.println("hi");
    }

    void sayHi(String name)         // name = "Mr.Ahmed"
    {
        System.out.println("hi " + name);
    }

    void printNumber() {
        System.out.println("No number");
    }



    void printNumber(int num) {
        System.out.println("Number :" + ++num);        // 101
    }



    void printNumber(Holder holder) {
        System.out.println("Holder number :" + ++holder.value);          // 101

    }

    void printNumber(int[] array) {
        for (int i = 0; i < array.length; i++)
            System.out.println("Holder number :" + ++array[i]);
    }

    void doSome(int a, double b, char c) {
        boolean bool= true ;
        if (bool)
            return;
        else
            System.out.println();

    }

    int RandomAction(int a, int b)
    {
        Random r = new Random();
        if (r.nextBoolean())
                return  a+ b ;
        return  a-b;

    }

    int doSome1(int a, double b, char c) {
        return (int) (a + b * c);

    }



    static String printArray(int[] arr)
    {
        StringBuilder builder = new StringBuilder("{") ;
        for(int i=0;i<arr.length;i++) {
            builder.append(arr[i]);
            if (i != arr.length - 1)
                builder.append(", ");
            else
                builder.append("}");
        }

        return builder.toString();

    }

    static void  changeNumber(int num)      // num =c
    {
        num++ ;
        System.out.println("changeNumber(): num = " + num);
    }
    static void  changeNumber(int[] nums)      // num =c
    {
        System.out.print("changeNumber()Array: nums = {" );
        for(int i=0;i<nums.length;i++) {
            nums[i]++;
            System.out.print(nums[i]+", ");
        }
        System.out.print("\b\b} \n");
    }

    static class Holder {
        int value;
    }

    static void  changeNumber(Holder h)
    {
        h.value++ ;
        System.out.println("changeNumber(): t.value = " + h.value);
    }

    static void  changeNumber1(Holder h)
    {
        Holder t = new Holder();
        t.value =h.value ;
        t.value++ ;
        System.out.println("changeNumber(): t.value = " + t.value);
    }

    // Syntax to create  : [specifier][access modifier]  <Return data type> method_name ([parameter list])  {  [statement]* }
    public static void main(String[] args) {

        // Note : ALL parameters in JAVA methods are passed by value, there is no pass my reference in Java
        int c = 10 ;
        System.out.println("c = " + c);             // 10
        changeNumber(c);                            // 11
        System.out.println("after calling changeNumber(c): c = " + c);      // 10

        int[] arr = {8,5,6};
        System.out.println("The array :"+ printArray(arr));         // {8, 5 ,6}
        changeNumber(arr);                                           // {9, 6 ,7}
        System.out.println("The array  after calling changeNumber(arr):"+ printArray(arr));  // {9, 6 ,7}

        Holder holder = new Holder();
        holder.value = 10 ;
        System.out.println("holder.value = " + holder.value);       // 10
        changeNumber(holder);                                       //  11
        System.out.println("holder.value after calling  hangeNumber(holder)= " + holder.value); // 11
//        Methods methods = new Methods();
//        methods.anotherSome();


//        methods.sayHi();
//        methods.sayHi("Ahmed");
//        String string = "Mr.Ahmed";
//        methods.sayHi(string);
//        methods.printNumber();
//        methods.printNumber(10);
//
//        int n = 100;
//        methods.printNumber(n);
//        System.out.println("n = " + n);         // 100
//
//        Holder h = new Holder();
//        h.value = 100;
//        methods.printNumber(h);
//        System.out.println("h.value = " + h.value);     // 100 , 101
//        staticDoSome();
//
//        int result = methods.doSome1(5, 15.3, 'A');
//
//        System.out.println("result = " + result);

    }
}
