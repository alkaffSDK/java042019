package basics;

import java.util.Scanner;

public class ControlOfFlow {

    /*
           1) if :

           syntax :     if(<boolean_expression>) statement; or {block} [else statement; or {block}]

           2) switch

            syntax :  switch(<switch_expression>)
            {   [case <unique_constant_value>: [statement]*]*
                [default: statement]*]
            }
            switch_expression : value of type : boolean, char , byte, short , int , String, or enum


            3) if-else-then (ternary operator)

            <boolean_expression> ? <value_if_true> : <value_if_false>

     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int x = 10;
        if (x ==5)
            System.out.println("Yes");
        else
            System.out.println("No");
        System.out.println("Done");


        final int c = 1;
        int a = scanner.nextInt();

        long lng = 10 ;
        switch (5) {
            case 0:
            case c + 2:
                System.out.println("One");
                break;
            case 2:
                System.out.println("zero");
                break;
            default:
                System.out.println("default");
        }


        /*
                35 - <50     -> Failed
                50 - <68     -> Passed
                68 - <76     -> Good
                76 - <84     -> Very Good
                84 - 100     -> Excellent
                other        -> Error

         */
        //a = 2 ;
        if (a == 2)
            System.out.println("Yes");
        else
            System.out.println("No");

        System.out.println("Done");

        int b = a >= 0 ? a : -a;
       System.out.println("b = " + (a >= 0 ? a : -a));
        System.out.println("b = " + b);

        System.out.println("mark:");
        double mark = scanner.nextDouble();

        if (mark < 35 || mark > 100)
            System.out.println("Error");
            // 35 <= mark <= 100
        else if (mark < 50)
            System.out.println("Failed");
            // 50 <= mark <= 100
        else if (mark < 68)
            System.out.println("Pass");
            // 68 <= mark <= 100
        else if (mark < 76)
            System.out.println("Good");
            // 76 <= mark <= 100
        else if (mark < 84)
            System.out.println("Very good");
            // 84 <= mark <= 100
        else
            System.out.println("Excellent");

        //<boolean_expression> ? <value_if_true> : <value_if_false>
        int m = x == 10 ? 10 : 20;

        System.out.println(
                mark < 35 || mark > 100 ? "Error" :
                        mark < 50 ? "Failed" :
                                mark < 68 ? "Passed" :
                                        mark < 76 ? "Good" :
                                                mark < 84 ? "Very good" :
                                                        "Excellent");
    }
}
