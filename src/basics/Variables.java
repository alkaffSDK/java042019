package basics;

import java.awt.*;
import java.util.ArrayList;
import java.util.Locale;

public class Variables {

    // Variable : is a symbolic name (an identifier) paired with a storage location (identified by a memory address)
    // which contains some known or unknown quantity of information referred to as a value.

    // Syntax: to define a variable
    //        [modifiers] [specifier] [specifier] <data_type> <variable_identifier> [=<expression>][, <variable_identifier> [=<expression>]]*;


    // Type of variables based it's location

    {


    }
    static  {
        System.out.println("hi");
    }
    public  void methed()
    {

    }
    // variable can be defined in one of three places (locations)

    // 1) inside a a block (like a method)           (local variable)
    // 2) inside an object          (instance variable)
    // 3) inside a class           (static / class variable)

    // Note : both 2 and 3 are *written* inside the class but the class variables are static

    // In Java local variables must be defined before been used.


    int instanceVariable;              // instance variable

    static int classVariable;              // class variable

    public static void main(String[] args) {


        ArrayList<String> stringArrayList = new ArrayList<>();
        stringArrayList.add("s");

        double k;
        k = 2;


        // Example :
        int x = 10;                     // x is a variable
        Object o = new Object() ;        // o is a variable


        // to use the variable

        System.out.println(x);


        // Identifier rules
        // 1) Contains [A-Z,a-z,0-9,_,$], because the symbols have it own role
        // int c-d = 10 ;
        // 2) start with  [A-Z,a-z,_,$], to differentiate between the number and the symbols
//        int x3 = 10 ;     // this is possible
//        int 3x = 20 ;     // if this was possible
//        int 33 = 30 ;     // then this will be possible
//        int d = 33 ;      // so what will be the value if d (33 or 30)
        // 3) not keyword
//         int int = 10 ;
//         int  = 20 ;


        int a = Math.abs(15 + 2);

        int m = 2, n= 3, r = 15;
        x = 20;
        int m3  = 20;

        // Zalid variable identifier
        int c$d, c_d;

        // Invalid variable identifier
        //  c-d , 3d , int

        System.out.println(33);





        int localVariable;


        // Data types            c++ : bool , char ,    , short, int ,long  ,float , double
        // 1) Primitive Data Types : boolean ,char ,byte, short, int ,long  ,float , double
        // 2) Reference/Object Data Types  : any other types


        boolean b = true;


        int c = 10 ;
        System.out.println("c = " + c);     // 10
        c = 010 ;
        System.out.println("c = " + c);     // 8
        c = 0x10 ;
        System.out.println("c = " + c);     // 16

        char ch = 'A';
        System.out.println("ch = ." + ch);
        ch = 65;
        System.out.println("ch = ." + ch);
        ch = 0101;
        System.out.println("ch = ." + ch);
        ch = 0x41;
        System.out.println("ch = ." + ch);
        ch = '\u0041';
        System.out.println("ch = ." + ch);


        // integers : byte ,short , int , long

        byte by = 127 ;        // -128 to 127
        short sho = 32767   ;       // -32768 to 32767
        int inte = 2147483647 ;     // -2147483648 to 2147483647
        long l = 9223372036854775807L;  // 9223372036854775807 ;        // long number (more than int range) must end with L/l

        float f = 10.54F;          // F , float numbers must end with f/F
        double d = 10.54;

        int val = 0x1abcdef;
        System.out.println("val = " + val);
        System.out.println("Integer.toBinaryString(val) = " + Integer.toBinaryString(val));
        System.out.println("Integer.toOctalString(val) = " + Integer.toOctalString(val));
        System.out.println("Integer.toHexString(val) = " + Integer.toHexString(val));

        // Type conventions :
        //  java can convert implicitly byte --> short--> int--> long --> float -->double


        // there are some risky cases :
        //      1) int--> float : for big integer value
        int value = 2147483640;
        float target = value;
        int back = (int) target;

        System.out.printf(Locale.getDefault(),"%10s = %15d%n" ,"Value", value);
        System.out.printf(Locale.getDefault(),"%10s = %15.0f%n" ,"target", target);
        System.out.printf(Locale.getDefault(),"%10s = %15d%n" ,"back", back);

        System.out.println("----------------------");
        //      2) long --> float : for big long value (Bigger than 4 bytes )
        long longValue = Long.MAX_VALUE - 10;
        target = longValue;
        long  Lback = (int) longValue;

        System.out.printf(Locale.getDefault(),"%15s = %15d%n" ,"longValue", longValue);
        System.out.printf(Locale.getDefault(),"%15s = %15.0f%n" ,"target", target);
        System.out.printf(Locale.getDefault(),"%15s = %15d%n" ,"Lback", Lback);
        System.out.println("----------------------");
        
        // 3) long --> double : for big long value

        double dubTraget = longValue;
        Lback = (long) dubTraget;
        Lback = Double.doubleToLongBits(dubTraget);



        System.out.printf(Locale.getDefault(),"%15s = %15d%n" ,"longValue", longValue);
        System.out.printf(Locale.getDefault(),"%15s = %15.0f%n" ,"target", target);
        System.out.printf(Locale.getDefault(),"%15s = %15d%n" ,"Lback", Lback);
        System.out.println("----------------------");
        // longValue = 10 ;

        int s = 1542;        // type casting


        System.out.println("s = " + s);
        System.out.println("Integer.toBinaryString(s) = " + Integer.toBinaryString(s));

        // Cast from string to integer
        s = Integer.valueOf("1254");        // or Integer.valueOf("1254", 10)
        System.out.println("s = " + s);

        s = Integer.valueOf("1254", 8);
        System.out.println("s = " + s);

        s = Integer.valueOf("1001", 2);
        System.out.println("s = " + s);

        s = Integer.valueOf("41A", 16);
        System.out.println("s = " + s);

    }

    //
/*
        000     0       0           0
        001     1       1           1
        010     2       2           2
        011     3       3           3
        100     4       -0          -4
        101     5       -1          -3
        110     6       -2          -2
        111     7       -3          -1

        100010010100
        011101101100
                    001
                    111
                    000
 */
}
