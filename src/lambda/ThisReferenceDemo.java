package lambda;


public class ThisReferenceDemo {
    interface Process {
        void process(int i);
    }


    public void doProcess(int i, Process p) {
        p.process(i);
    }
    public void execute() {
        doProcess(10, i -> {
            System.out.println("Value of i is " + i);
            System.out.println(this);
        });
    }

    public static void main(String[] args) {
        ThisReferenceDemo thisReferenceDemo = new ThisReferenceDemo();
        thisReferenceDemo.foo();

//        thisReferenceDemo.doProcess(10, i -> {
//			System.out.println("Value of i is " + i);
//			System.out.println(this);
//		});
//
//
//        thisReferenceDemo.doProcess(10, new Process() {
//            @Override
//            public void process(int i) {
//                System.out.println("Value of i is " + i);
//                System.out.println(this);
//            }
//        });

       // thisReferenceDemo.execute();


    }

    public void foo()
    {
        ThisReferenceDemo thisReferenceDemo = new ThisReferenceDemo();

        System.out.println(this);
        thisReferenceDemo.doProcess(10, i -> {
            System.out.println("Value of i is " + i);
            System.out.println(this);           //This is the main ThisReference class instance"
        });


        thisReferenceDemo.doProcess(10, new Process() {
            @Override
            public void process(int i) {
                System.out.println("Value of i is " + i);
                System.out.println(this);
            }

            @Override
            public String toString() {
                return "This is the  inner type";
            }
        });
    }

    public String toString() {
        return "This is the ThisReferenceDemo class instance";
    }

}



