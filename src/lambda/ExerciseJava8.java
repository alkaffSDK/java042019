package lambda;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;


public class ExerciseJava8 {

	public static void main(String[] args) {
		// 1: Create list of person in the class
		List<Person> people = Arrays.asList(
				new Person("Ahmed", "Alkaff", 34),
				new Person("Raghad", "Mando", 22),
				new Person("Omama", "Manaa", 26),
				new Person("Yazan", "AbuHamdodeh", 26),
				new Person("Imaseil", "Bibers", 23),
				new Person("Ahmed", "Dosogqei", 25),
				new Person("Yaser", "Meryan", 23)

		);

		// 2: Sort list by last name

//		// Java 7
//		Collections.sort(people, new Comparator<Person>() {
//			@Override
//			public int compare(Person o1, Person o2) {
//				return o1.getLastName().compareTo(o2.getLastName());
//			}
//		});

		// Java 8

		Collections.sort(people, (p1, p2) -> p1.getLastName().compareTo(p2.getLastName()));

		// 3: Create a method that prints all elements in the list

//		// Java 7
//		System.out.println("\nPrinting all persons in the class");
//		printAll(people);

		// Java 8

		System.out.println("\nPrinting all persons in the class");
//		printAll(people);
		printConditionally(people,(p) -> true ,System.out::println);

		// 4: Create a method that prints all people that have last name beginning with M

//		// Java 7
//		System.out.println("\nPrinting all persons with last name beginning with M");
//		printConditionally(people, new Condition() {
//			@Override
//			public boolean test(Person p) {
//				return p.getLastName().startsWith("M");
//			}
//		});


		// Java 8

		System.out.println("Printing all persons with last name beginning with M");
		printConditionally(people, p -> p.getLastName().startsWith("M"),p -> System.out.println(p));

		System.out.println("Printing all persons with first name beginning with M");
		printConditionally(people, p -> p.getFirstName().startsWith("M"),p -> System.out.println(p));
	}

//	private static void printAll(List<Person> people) {
//		for (Person p : people) {
//			System.out.println(p);
//		}
//
//	}
	private static void printConditionally(List<Person> people, Condition condition, Consumer<Person> consumer) {
		for (Person p : people) {
			if (condition.test(p)) {
				consumer.accept(p);
			}

		}

	}


	interface Condition {
		boolean test(Person p);
	}

}
