package lambda;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.LongUnaryOperator;
import java.util.function.Predicate;

public class StandardFunctionalInterfacesDemo {

	public static void main(String[] args) {
		List<Person> people = Arrays.asList(
				new Person("Ahmed", "Alkaff", 34),
				new Person("Raghad", "Mando", 22),
				new Person("Omama", "Manaa", 26),
				new Person("Yazan", "AbuHamdodeh", 26),
				new Person("Imaseil", "Bibers", 23),
				new Person("Mhmed", "Dosogqei", 25),
				new Person("Yaser", "Meryan", 23)
				);
		
		// todo: 2: Sort list by last name
		Collections.sort(people, (p1, p2) -> p1.getLastName().compareTo(p2.getLastName()));
		
		// todo: 3: Create a method that prints all elements in the list
		System.out.println("Printing all persons");
		performConditionally(people, p -> true, p -> System.out.println(p));
		
		// todo: 4: Create a method that prints all people that have last name beginning with M
		System.out.println("Printing all persons with last name beginning with M");
		performConditionally(people, p -> p.getLastName().startsWith("M"), p -> System.out.println(p));

		System.out.println("Printing all persons with first name beginning with M");
		
		performConditionally(people, p -> p.getFirstName().startsWith("M"), p -> System.out.println(p.getFirstName()));



		Predicate<Person> personPredicate = new Predicate<Person>() {
			@Override
			public boolean test(Person person) {
				return false;
			}
		};

		Consumer<Person> personConsumer = new Consumer<Person>() {
			@Override
			public void accept(Person person) {

			}
		};
	}

	private static void performConditionally(List<Person> people, Predicate<Person> predicate, Consumer<Person> consumer) {
		for (Person p : people) {
			if (predicate.test(p)) {
				consumer.accept(p);
			}
		}
	}

	private  static void printAll(List<Person> list)
	{
		for (Person p: list) {
			System.out.println(p);
		}
	}

	private  static void printAllStartWithM(List<Person> list)
	{
		for (Person p: list) {
			if(p.getLastName().toUpperCase().startsWith("M"))
			System.out.println(p);
		}
	}


	static class Holder {

	}
	interface  MyInterface<T1, T2,T3,T4,R> {
		R action(T1 t1, T2 t2, T3 t3, T4 t4 );
	}
}
