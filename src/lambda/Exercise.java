package lambda;

import java.util.*;
import java.util.function.Predicate;


public class Exercise {

	public static void main(String[] args) {
		// 1: Create list of person in the class
		List<Person> people = Arrays.asList(
				new Person("Ahmed", "Alkaff", 34),
				new Person("Raghad", "Mando", 22),
				new Person("Omama", "Manaa", 26),
				new Person("Yazan", "AbuHamdodeh", 26),
				new Person("Imaseil", "Bibers", 23),
				new Person("Ahmed", "Dosogqei", 25),
				new Person("Yaser", "Meryan", 23)

		);

		// 2: Sort list by last name


		Collections.sort(people, new Comparator<Person>() {
			@Override
			public int compare(Person o1, Person o2) {
				return Integer.compare(o1.getAge(), o2.getAge());
			}
		});

		Collections.sort(people, (Person o1, Person o2) -> {return Integer.compare(o1.getAge(), o2.getAge());});
		Collections.sort(people, ( o1,  o2) ->  Integer.compare(o1.getAge(), o2.getAge()));
		Collections.sort(people, Comparator.comparingInt(Person::getAge));


		// 3: Create a method that prints all elements in the list

		System.out.println("\nPrinting all persons in the class");
		printAll(people);

		// 4: Create a method that prints all people that have last name beginning with M

		System.out.println("\nPrinting all persons with last name beginning with M");

		printAllWithCondition(people, new Condition<Person>() {
			@Override
			public boolean test(Person person) {
				return person.getAge()> 20;
			}
		});


		printAllWithCondition(people, (p) ->p.getAge()> 20);

		printAllWithCondition(people, p ->p.getAge()< 30);


		printAllWithConditionAndAction(people, p -> p.getAge() > 20, (p) -> System.out.println("p = " + p) ) ;
		ArrayList<Person> arrayList = new ArrayList<>();
		printAllWithConditionAndAction(people, p -> p.getAge() > 20, (p) -> arrayList.add(p)) ;


		printConditionally(people, new Predicate<Person>() {
			@Override
			public boolean test(Person p) {
				return p.getLastName().startsWith("M");
			}
		});
	}

	private static void printAll(List<Person> people) {
		for (Person p : people) {
			System.out.println(p);
		}

	}

	private static void printAllLastName(List<Person> people) {
		for (Person p : people) {
			if(p.getLastName().toUpperCase().startsWith("M"))
				System.out.println(p);
		}

	}

	private static void printAllWithCondition(List<Person> people, Condition<Person> condition) {
		for (Person p : people) {
			if(condition.test(p))
				System.out.println(p);
		}
	}

	private static void printAllWithConditionAndAction(List<Person> people, Condition<Person> condition, Action<Person> action) {
		for (Person p : people) {
			if(condition.test(p))
				action.accept(p);
		}

	}


	private static void printConditionally(List<Person> people, Predicate<Person> predicate) {
		for (Person p : people) {
			if (predicate.test(p)) {
				System.out.println(p);
			}

		}

	}


	interface Condition<T> {
		boolean test(T t);
	}

	interface Action<T> {
		void accept(T t);
	}



}
