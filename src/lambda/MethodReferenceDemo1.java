package lambda;

public class MethodReferenceDemo1 {

    public static void main(String[] args) {

        Thread t1 = new Thread(MethodReferenceDemo1::printMessage, "Worker");
        String name = "Ahmed";


//		Thread thread = new Thread(() -> System.out.println(Thread.currentThread().getName() +" :Hello"),"Worker");
//		thread.start();
//
//
//		Thread thread1 = new Thread(() -> printMessage() ,"Worker");
//		thread1.start();
//
//		Thread thread2 = new Thread(MethodReferenceDemo1::printMessage ,"Worker");
//		thread1.start();
//
//		MethodReferenceDemo1 obj = new MethodReferenceDemo1();
//		Thread thread3 = new Thread(obj::InstanceprintMessage ,"Worker");
//		thread1.start();
    }


    public static void printMessage() {
        System.out.println(Thread.currentThread().getName() +" :Hello");
    }


    public void InstanceprintMessage() {
        System.out.println(Thread.currentThread().getName() + " :InstanceprintMessage Hello");
    }

}
