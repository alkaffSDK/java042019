package lambda;

public class IntroToLambda {


    public static void main(String[] args) {

        // to run any code in a new thread we need to put this code at run method
        // so we need to create an object and then send that object to the thread
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {

            }
        });

        Thread t1 = new Thread(() -> {
            System.out.println("t = " + t);
        });
        t1.start();


        // Lambda expression is an reference to a method

        // Lambda expression can only be used with interface that contains one and only one abstract method
        MyInterface r = () -> {
            System.out.println("statement 1");
            System.out.println("statement 2");
        };

        // for method with single statements you can remove {} and ;
        MyInterface r1 = () -> System.out.println();


        // if the function has a return value
        MyInterface1 r2 = () -> {
            System.out.println();
            return true;
        };

        // if it is one statement you can remove the return keyword
        MyInterface1 r3 = () -> {
            return true;
        };

        MyInterface1 r4 = () -> true;

        // if the function has an input parameters

        MyInterface2 r5 = (int a) -> {
            System.out.println("a = " + a);
        };

        MyInterface2 r6 = (int a) -> System.out.println("a = " + a);

        MyInterface2 r7 = (a) -> System.out.println("a = " + a);

        MyInterface2 r8 = a -> System.out.println("a = " + a);

        MyInterface3 r9 = (a, b) -> System.out.println(a + b);

        MyInterface4 r10 = (a, b) -> a + b;

        // Can't use Lambda with abstract classes, because, it is a class am could contains data so you have to create an object
        // Test te = ()-> {};

//        function var = public void run() {
//
//        } ;
    }

    static abstract  class Test {
        public  abstract void foo();
    }

    interface MyInterface {
        void foo();
        default void doo(int a) {
        }
    }

    @FunctionalInterface
    interface MyInterface1 {
        boolean foo();

    }

    @FunctionalInterface
    interface MyInterface2 {
        void foo(int a);

    }

    interface MyInterface3 {
        void foo(int a, String b);

    }

    interface MyInterface4 {
        String foo(int a, String b);

    }
}
