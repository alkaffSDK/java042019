package lambda;

import java.util.*;
import java.util.function.*;

public class StreamsDemo {

    public static void iterate(List<Person> list, Predicate<Person> op1, Predicate<Person> op2, Consumer<Person> consumer) {
        for (Person p : list) {
            if (op1.test(p) && op2.test(p))
                consumer.accept(p);

        }
    }

    public static void main(String[] args) {

        Random r = new Random();
        List<Person> people = new LinkedList<>();
        people.add(new Person("Ahmed", "Alkaff", 34));
        people.add(new Person("Raghad", "Mando", 22));
        people.add(new Person("Yazan", "AbuHamdodeh", 26));
        people.add(new Person("Imaseil", "Bibers", 23));
        people.add(new Person("Ahmed", "Dosogqei", 25));
        people.add(new Person("Yaser", "Meryan", 23));

        System.out.println("===================");
        people.stream().mapToInt(new ToIntFunction<Person>() {
            @Override
            public int applyAsInt(Person value) {
                return value.getAge();
            }
        }).forEach(new IntConsumer() {
            @Override
            public void accept(int value) {
                System.out.println(value);
            }
        });
        System.out.println("===================");
        people.stream().mapToInt(p->p.getAge()).forEach(p -> System.out.println(p) );
        System.out.println("===================");
        people.stream().mapToInt(p->p.getAge()).forEach(System.out::println );

        System.out.println("===================");
        people.stream().mapToInt(p->p.getAge()).forEach( t-> System.out.println(t));

        Optional<Person> op = people.stream()
                .filter(s -> s.getAge() < 25)
                .findAny();

        System.out.println("op = " + op);
        people.stream()
                .collect(ArrayList<String>::new, (a, p) -> a.add(p.getLastName() + ":" + p.getAge()), (a1, a2) -> a1.addAll(a2))
                .stream()
                .filter(s ->s.length() > 10)
                .forEach(System.out::println);

        //people.stream().map(person -> person.getAge()).forEach(System.out::println);

//        Thread t = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                System.out.println();
//            }
//        });
//
//
//        for (int i = 0; i < 1000; i++) {
//            Person p = null;
//
//            try {
//                p = people.get(0).clone();
//                //System.out.println((char)('a' + r.nextInt('Z'-'A'+1)));
//
//                p.setFirstName((char) ('A' + r.nextInt(27)) + p.getFirstName());
//                p.setLastName((char) ('A' + r.nextInt(27)) + p.getLastName());
//
//                people.add(p);
//            } catch (CloneNotSupportedException e) {
//                e.printStackTrace();
//            }
//
//        }

//        long start1 = System.nanoTime();
//
//        for (int i = 0; i < people.size(); i++) {
//            System.err.println(Thread.currentThread().getName()+ ": " + people.get(i));
//        }
//        long end1 = System.nanoTime()-start1;
//
//        long start2 = System.nanoTime();
//        for (Person p : people) {
//            System.err.println(Thread.currentThread().getName()+ ": " +p);
//        }
//
//        long end2 = System.nanoTime()-start2;


//        long start3 = System.nanoTime();
//        people.parallelStream().forEach((p) -> System.err.println(Thread.currentThread().getName() + ": " + p));
//        long end3 = System.nanoTime() - start3;

//        long start4 = System.nanoTime();
//        people.stream().forEach(System.err::println);
//        long end4 = System.nanoTime()-start4;


//        System.out.println("for loop time     :"+end1);
//        System.out.println("foreach time      :"+end2);
        // System.out.println("stream  time      :"+end4);
//        System.out.println("stream time      :" + end3);


//        iterate(people,p -> p.getLastName().startsWith("M"),p->p.getFirstName().contains("a"),p -> System.out.println(p));
//
//        System.out.println("-----------------------------------------------------");
//        people.stream()
//                .filter(p -> p.getLastName().startsWith("M"))
//                //.filter(p -> p.getFirstName().contains("a"))
//                .forEach(p -> System.out.println(Thread.currentThread().getName()+ ": " +p));
//
//        System.out.println("stream time      :"+end3);
////
//		for(int i=0;i<100;i++)
//		{
//			Person p = null;
//
//			try {
//				p = people.get(0).clone();
//				//System.out.println((char)('a' + r.nextInt('Z'-'A'+1)));
//
//				p.setFirstName((char)('A' + r.nextInt(27))+ p.getFirstName());
//				p.setLastName((char)('A' + r.nextInt(27))+ p.getLastName());
//
//				people.add(p);
//			} catch (CloneNotSupportedException e) {
//				e.printStackTrace();
//			}
//
//		}

//		long start = System.nanoTime();
//
//		people.parallelStream()
//				.filter(p -> p.getLastName().startsWith("M"))
//				.forEach(p -> System.out.println(Thread.currentThread().getName()+":"+p.getFirstName()));
//
//		System.out.println("Ends on :"+(System.nanoTime()-start));
//
//		long start1 = System.nanoTime();
//		people.stream()
//				.filter(p -> p.getLastName().startsWith("M"))
//				.forEach(p -> System.out.println(Thread.currentThread().getName()+":"+p.getFirstName()));
//
//		System.out.println("Ends on :"+(System.nanoTime()-start));
//
//
//		long count = people.parallelStream()
//		.filter(p -> p.getLastName().startsWith("D"))
//		.count();
//
        //System.out.println(count);


    }

}
