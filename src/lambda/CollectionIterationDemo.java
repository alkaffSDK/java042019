package lambda;

import java.util.Arrays;
import java.util.List;

public class CollectionIterationDemo {

	public static void main(String[] args) {
		List<Person> people = Arrays.asList(
				new Person("Ahmed", "Alkaff", 34),
				new Person("Raghad", "Mando", 22),
				new Person("Omama", "Manaa", 26),
				new Person("Yazan", "AbuHamdodeh", 26),
				new Person("Imaseil", "Bibers", 23),
				new Person("Ahmed", "Dosogqei", 25),
				new Person("Yaser", "Meryan", 23)
				);
		System.out.println("Using for loop");
		
		for (int i = 0; i < people.size(); i++) {
			System.out.println(people.get(i));
		}
		
		System.out.println("Using for in loop");
		
		for (Person p : people) {
			System.out.println(p);
		}
		
		System.out.println("Using lambda for each loop");
		people.forEach(System.out::println);
		
		

	}

}
