package lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.UnaryOperator;

public class ExamplesOfLambda {

    public static void main(String[] args) {


        List<Integer> integers = Arrays.asList(10,15,17,165,15,69,1254,326,2,5,3,2,5,5,5,2,655,6,2,655,854);

        Consumer<Integer> consumer  = new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) {
                System.out.print(integer+ ", ");
            }
        };


        UnaryOperator<Integer> uni = new UnaryOperator<Integer>() {
            @Override
            public Integer apply(Integer integer) {
                return null;
            }
        };

        integers.forEach(i -> System.out.print(i+", "));
        integers.replaceAll(i -> i/2);

        integers.forEach(i -> System.out.print(i+", "));
        System.out.println();

        integers.forEach(consumer);

        System.out.println();

        integers.forEach(i -> System.out.print(i+ ", "));


//        String[] arr = new String[integers.size()];
//        integers.toArray(arr);


        System.out.println();
        integers.stream().filter( i -> i % 2 == 0 ).distinct().forEach( i-> System.out.print(i + ", ") );

        System.out.println();
        integers.stream().filter( i -> i % 2 == 0 ).forEach( i-> System.out.print(i + ", ") );

        System.out.println();
        int max  = integers.stream().max( Comparator.naturalOrder()).get();

        System.out.println("max = " + max);


    }
}
